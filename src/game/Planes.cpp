#include <string>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "Box.h"
#include "Dimension.h"
#include "Environment.h"
#include "Font.h"
#include "FontReader.h"
#include "FontRenderer.h"
#include "Game.h"
#include "GameInput.h"
#include "GameOutput.h"
#include "GameState.h"
#include "ImageService.h"
#include "Rectangle.h"
#include "Scene.h"
#include "SceneElement.h"
#include "Sprite.h"
#include "SpriteManager.h"

// The attributes of the screen
const int SCREEN_WIDTH = 320;
const int SCREEN_HEIGHT = 240;
const int SCREEN_BPP = 32;

/** The frames per second */
const int FRAMES_PER_SECOND = 20;
/** How long a frame should take */
const int PERIOD = 1000 / FRAMES_PER_SECOND;
/**
 * Number of frames with a delay of 0 ms before the
 * animation thread yields to other running threads
 */
const unsigned int NO_DELAYS_PER_YIELD = 16;
/**
 * Number of frames that can be skipped in one animation loop
 * i.e. the games state is updated but not rendered
 */
const int MAX_FRAME_SKIPS = 5;

int resolutionFactor = 1;

static const char* WM_CAPTION = "Battle Plane";

SDL_Surface* screen = NULL;
SDL_Surface* icon = NULL;
SpriteManager sm;
Game game;
ImageService imageService;
FontRenderer fontRenderer;
SDL_Surface* gameOverText;

bool quit = false;

bool init() {
	if(SDL_Init(SDL_INIT_VIDEO) == -1) {
		return false;
	}
	// Set up the screen
	// SDL_WM_SetIcon must be called before SDL_SetVideoMode
	std::string filename = Environment::getResourcePath("icon32.bmp");
	icon = SDL_LoadBMP(filename.c_str());
	SDL_WM_SetIcon(icon, NULL);
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);
	if( screen == NULL ) {
		return false;
	}
	// Set the window caption
	SDL_WM_SetCaption(WM_CAPTION, NULL);

	return true;
}

void loadSpriteSheets() {
	std::string filename;
	filename = Environment::getResourcePath("sky");
	sm.readSpritesheet(filename);
	filename = Environment::getResourcePath("planes");
	sm.readSpritesheet(filename);
}

void loadFont() {
	std::string filename;
	filename = Environment::getResourcePath("default_font.png");
	SDL_Surface* fontSurface = imageService.loadImage(filename);

	SDL_Color bg = {0, 0xff, 0xff, 0};

	FontReader fontReader;
	fontReader.setSurface(fontSurface);
	fontReader.setBgColor(bg);
	Font f = fontReader.getFont();
	fontRenderer.setFont(f);

	SDL_FreeSurface(fontSurface);
}

void loadText() {
	gameOverText = fontRenderer.renderText("game over");
}

bool loadFiles() {
	loadSpriteSheets();
	loadFont();
	loadText();

	return true;
}

void cleanUp() {
	SDL_FreeSurface(gameOverText);
	SDL_FreeSurface(icon);
	SDL_FreeSurface(screen);

	SDL_Quit();
}

void onKeyDown(SDL_Event* event) {
	GameInput& gameInput = game.getInput();
	switch( event->key.keysym.sym ) {
	case SDLK_UP:
		gameInput.startUp();
		break;
	case SDLK_DOWN:
		gameInput.startDown();
		break;
	case SDLK_LEFT:
		gameInput.startLeft();
		break;
	case SDLK_RIGHT:
		gameInput.startRight();
		break;
	case SDLK_SPACE:
		gameInput.startFire();
		break;
	default:
		break;
	}
}

void onKeyUp(SDL_Event* event) {
	GameInput& gameInput = game.getInput();
	switch( event->key.keysym.sym ) {
	case SDLK_UP:
		gameInput.endUp();
		break;
	case SDLK_DOWN:
		gameInput.endDown();
		break;
	case SDLK_LEFT:
		gameInput.endLeft();
		break;
	case SDLK_RIGHT:
		gameInput.endRight();
		break;
	case SDLK_SPACE:
		gameInput.endFire();
		break;
	default:
		break;
	}
}

void gameUpdate() {
	game.update();
}

void displayGameOver() {
	// Center on screen
	Dimension textBox = fontRenderer.sizeText("game over");
	int textX = (SCREEN_WIDTH - textBox.getWidth()) / 2;
	int textY = (SCREEN_HEIGHT - textBox.getHeight()) / 2;
	// Paint the text
	imageService.applySurface(textX, textY, gameOverText, screen);
}

void gameRender() {
	const GameOutput& gameOutput = game.getOutput();
	int count = gameOutput.getScene().elementCount();
	SDL_Surface* surf = NULL;
	SDL_Rect* clip = NULL;
	int x = 0;
	int y = 0;
	for(int i = 0; i < count; i++) {
		const SceneElement& e = gameOutput.getScene().getElementAt(i);
		const Sprite& sprite = sm.getSprite(e.getAppearance());
		surf = sprite.getSurface();
		clip = sprite.getRectangle();
		x = e.getBox().getX() * resolutionFactor;
		y = e.getBox().getY() * resolutionFactor;

		imageService.applySurface(x, y, surf, screen, clip);
	}

	if(game.getState() == GameState::OVER) {
		displayGameOver();
	}
}

void paintScreen() {
	if(SDL_Flip(screen) == -1) {
		printf("can't flip!\n");
	}
}

void handleEvents() {
	// The event structure that will be used
	SDL_Event event;
	// While there's an event to handle
	while( SDL_PollEvent( &event ) ) {
		switch(event.type) {
		case SDL_KEYDOWN:
			// A key was pressed
			onKeyDown(&event);
			break;
		case SDL_KEYUP:
			// A key was released
			onKeyUp(&event);
			break;
		case SDL_QUIT:
			// The user has Xed out the window
			// Quit the program
			quit = true;
			break;
		default:
			break;
		}
	}
}

int main() {
	// Initialize
	if(init() == false) {
		return 1;
	}
	// Load the files
	if(loadFiles() == false) {
		return 1;
	}
	resolutionFactor = SCREEN_WIDTH / Scene::WIDTH;
	printf("resolution factor: %d\n", resolutionFactor);
	game.start();
	Uint32 beforeTime = 0;
	Uint32 afterTime = 0;
	Uint32 timeDiff = 0;
	Sint32 sleepTime = 0;
	Sint32 overSleepTime = 0;
	Sint32 excess = 0;
	Uint32 noDelays = 0;
	// While the user hasn't quit
	while(quit == false) {
		beforeTime = SDL_GetTicks();

		handleEvents();
		gameUpdate(); // game state is updated
		gameRender(); // render to buffer
		paintScreen(); // paint with the buffer

		afterTime = SDL_GetTicks();
		timeDiff = afterTime - beforeTime;
		sleepTime = (PERIOD - timeDiff) - overSleepTime; // time left in this loop
		if(sleepTime > 0) {
			// Some time left in this loop
			SDL_Delay(sleepTime);
			overSleepTime = (SDL_GetTicks() - afterTime) - sleepTime;
		} else {
			// sleepTime <= 0; frame took longer than the period
			excess -= sleepTime; // store excess time value
			overSleepTime = 0;
			if(++noDelays >= NO_DELAYS_PER_YIELD) {
				// Give another thread a chance to run
				SDL_Delay(1); // instead of thread yielding
				noDelays = 0;
			}
		}
		/* If frame animation is taking too long, update the game state
		 * without rendering it, to get the updates/sec nearer to
		 * the required FPS.
		 */
		int skips = 0;
		while((excess > PERIOD) && (skips < MAX_FRAME_SKIPS)) {
			excess -= PERIOD;
			gameUpdate(); // update state but don't render
			skips++;
			printf("*** skipping frame\n");
		}
	}
	cleanUp();
	return 0;
}
