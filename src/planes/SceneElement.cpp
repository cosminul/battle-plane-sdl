#include "Box.h"
#include "SceneDispatcher.h"
#include "SceneElement.h"

SceneElement::SceneElement(const std::string& kind, const std::string& name):
kind(kind),
name(name),
appearance(),
currentAppearance(),
box(new Box),
dispatcher(NULL),
moveLeft(false),
moveRight(false),
moveUp(false),
moveDown(false),
anchored(false),
speedFactor(1),
absoluteSpeed(0),
actualSpeed(1),
actualSpeedY(1)
{
}

SceneElement::SceneElement(const SceneElement& other):
kind(other.kind),
name(other.name),
appearance(other.appearance),
currentAppearance(other.currentAppearance),
box(new Box(*other.box)),
dispatcher(other.dispatcher),
moveLeft(other.moveLeft),
moveRight(other.moveRight),
moveUp(other.moveUp),
moveDown(other.moveDown),
anchored(other.anchored),
speedFactor(other.speedFactor),
absoluteSpeed(other.absoluteSpeed),
actualSpeed(other.actualSpeed),
actualSpeedY(other.actualSpeedY)
{
}

SceneElement::~SceneElement() {
	delete box;
}

SceneElement& SceneElement::operator=(const SceneElement& other) {
	if(this != &other) {
		kind.assign(other.kind);
		name.assign(other.name);
		appearance = other.appearance;
		currentAppearance.assign(other.currentAppearance);
		delete box;
		box = new Box(*other.box);
		dispatcher = other.dispatcher;
		moveLeft = other.moveLeft;
		moveRight = other.moveRight;
		moveUp = other.moveUp;
		moveDown = other.moveDown;
		anchored = other.anchored;
		speedFactor = other.speedFactor;
		absoluteSpeed = other.absoluteSpeed;
		actualSpeed = other.actualSpeed;
		actualSpeedY = other.actualSpeedY;
	}
	return *this;
}

const std::string& SceneElement::getKind() const {
	return kind;
}

const std::string& SceneElement::getName() const {
	return name;
}

const std::string& SceneElement::getAppearance() const {
	return currentAppearance;
}

void SceneElement::setAppearance(const std::vector<std::string>& appearance) {
	this->appearance = appearance;
	if(appearance.size() > 0) {
		this->currentAppearance.assign(appearance[0]);
	}
}

void SceneElement::setDispatcher(SceneDispatcher& dispatcher) {
	this->dispatcher = &dispatcher;
}

Box& SceneElement::getBox() const {
	return *box;
}

int SceneElement::getReferenceSpeed() const {
	return speedFactor * absoluteSpeed;
}

void SceneElement::startMoving(int direction) {
	if(direction & LEFT) {
		moveLeft = true;
		speedFactor = -1;
	}
	if(direction & RIGHT) {
		moveRight = true;
		speedFactor = 1;
	}
	if(direction & UP) {
		moveUp = true;
	}
	if(direction & DOWN) {
		moveDown = true;
	}
}

void SceneElement::stopMoving(int direction) {
	if(direction & LEFT) {
		moveLeft = false;
	}
	if(direction & RIGHT) {
		moveRight = false;
	}
	if(direction & UP) {
		moveUp = false;
	}
	if(direction & DOWN) {
		moveDown = false;
	}
}

void SceneElement::stopMoving() {
	moveLeft = false;
	moveRight = false;
	moveUp = false;
	moveDown = false;
}

void SceneElement::updatePosition() {
	if(moveLeft) {
		box->moveX(actualSpeed);
	}
	if(moveRight) {
		box->moveX(actualSpeed);
	}
	if(moveUp) {
		box->moveY(-actualSpeedY);
	}
	if(moveDown) {
		box->moveY(actualSpeedY);
	}
}

void SceneElement::updateAppearance() {
}

void SceneElement::adjustSpeed(int reference) {
	actualSpeed = speedFactor * absoluteSpeed - reference;
}

void SceneElement::die() {
	// Move it outside the scene so it can be garbage collected
	box->setX(-2 * box->getWidth());
	box->setY(-2 * box->getHeight());
}

bool SceneElement::isAnchored() const {
	return anchored;
}

void SceneElement::setAnchored(bool anchored) {
	this->anchored = anchored;
}

void SceneElement::setVerticalSpeed(int speed) {
	actualSpeedY = speed;
}
