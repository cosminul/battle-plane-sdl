#ifndef GAMEINPUT_H
#define GAMEINPUT_H

#include <deque>

class InputListener;

class GameInput {
public:
	GameInput();
	~GameInput();
	void addListener(InputListener* listener);

	void startLeft();
	void endLeft();
	void startRight();
	void endRight();
	void startUp();
	void endUp();
	void startDown();
	void endDown();
	void startFire();
	void endFire();

private:
	std::deque<InputListener*> listeners;
};

#endif // GAMEINPUT_H
