#include "Background.h"
#include "BackgroundScroller.h"
#include "Box.h"
#include "Direction.h"
#include "Scene.h"

BackgroundScroller::BackgroundScroller():
bg1(new Background("Sky", "sky")),
bg2(new Background("Sky", "sky")),
scene(NULL)
{
	bg1->getBox().setPosition(0, 0);
	bg2->getBox().setPosition(bg1->getBox().getWidth(), 0);

	// Prevent the background from being garbage collected
	// while one of them is outside the scene
	bg1->setAnchored(true);
	bg2->setAnchored(true);

	bg1->startMoving(LEFT);
	bg2->startMoving(LEFT);
}

BackgroundScroller::BackgroundScroller(const BackgroundScroller& other):
bg1(new Background(*other.bg1)),
bg2(new Background(*other.bg2)),
scene(other.scene)
{
}

BackgroundScroller& BackgroundScroller::operator=(const BackgroundScroller& other) {
	if(this != &other) {
		delete bg1;
		bg1 = new Background(*other.bg1);
		delete bg2;
		bg2 = new Background(*other.bg2);
		scene = other.scene;
	}
	return *this;
}

BackgroundScroller::~BackgroundScroller() {
	scene->removeElement(*bg2);
	delete bg2;
	scene->removeElement(*bg1);
	delete bg1;
}

void BackgroundScroller::setScene(Scene& scene) {
	this->scene = &scene;
	this->scene->addElement(*bg1);
	this->scene->addElement(*bg2);
}

void BackgroundScroller::update() {
	if(bg1->getBox().getX() == 0) {
		bg2->getBox().setX(bg1->getBox().getWidth());
	}
	if(bg2->getBox().getX() == 0) {
		bg1->getBox().setX(bg2->getBox().getWidth());
	}
}
