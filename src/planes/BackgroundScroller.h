#ifndef BACKGROUNDSCROLLER_H
#define BACKGROUNDSCROLLER_H

class Background;
class Scene;

class BackgroundScroller {
public:
	BackgroundScroller();
	BackgroundScroller(const BackgroundScroller& other);
	/**
	 * Copy assignment operator.
	 *
	 * @param other another BackgroundScroller
	 */
	virtual BackgroundScroller& operator=(const BackgroundScroller& other);
	virtual ~BackgroundScroller();
	void setScene(Scene& scene);
	void update();

private:
	Background* bg1;
	Background* bg2;
	Scene* scene;
};

#endif // BACKGROUNDSCROLLER_H
