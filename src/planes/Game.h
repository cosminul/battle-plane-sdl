#ifndef GAME_H
#define GAME_H

#include "GameState.h"
#include "InputListener.h"

class BackgroundScroller;
class GameInput;
class GameOutput;
class LevelRunner;
class Plane;
class SceneElement;

class Game: public InputListener {
public:
	Game();
	Game(const Game& other);
	Game& operator=(const Game& other);
	virtual ~Game();

	GameInput& getInput() const;
	const GameOutput& getOutput() const;
	void start();
	void update();
	GameState getState() const;

private:
	void handleCollision(SceneElement& first, SceneElement& second);
	void decideGameOver();

private:
	GameInput* input;
	GameOutput* output;
	GameState state;
	BackgroundScroller* bgScroll;
	LevelRunner* level;
	Plane* hero;
	static const int MAX_LINE_LEN;

public: // InputListener implementation
	virtual void onStartLeft();
	virtual void onEndLeft();
	virtual void onStartRight();
	virtual void onEndRight();
	virtual void onStartUp();
	virtual void onEndUp();
	virtual void onStartDown();
	virtual void onEndDown();
	virtual void onStartFire();
	virtual void onEndFire();
};

#endif // GAME_H
