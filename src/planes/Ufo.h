#ifndef UFO_H
#define UFO_H

#include <string>

#include "SceneElement.h"

class Ufo: public SceneElement {
public:
	Ufo(const std::string& kind, const std::string& name);

	virtual void updateAppearance();
	virtual void die();

private:
	int appearanceIndex;
	int updatesSinceLastChange;
	static const int UPDATES_BETWEEN_APPEARANCE_CHANGES;
};

#endif // UFO_H
