#ifndef DIRECTION_H
#define DIRECTION_H

enum Direction {
	NO_DIRECTION = 0x0,
	LEFT = 0x1,
	RIGHT = 0x2,
	UP = 0x4,
	DOWN = 0x8
};

#endif // DIRECTION_H
