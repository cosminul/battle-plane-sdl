#include "Rectangle.h"

Rectangle::Rectangle():
left(0),
top(0),
right(-1),
bottom(-1)
{
}

Rectangle::Rectangle(int x, int y, int w, int h):
left(x),
top(y),
right(x + w - 1),
bottom(y + h - 1)
{
}

Rectangle::Rectangle(const Rectangle &other):
left(other.left),
top(other.top),
right(other.right),
bottom(other.bottom)
{
}

Rectangle::~Rectangle() {
}

Rectangle& Rectangle::operator=(const Rectangle& other) {
	if(this != &other) {
		left = other.left;
		top = other.top;
		right = other.right;
		bottom = other.bottom;
	}
	return *this;
}

int Rectangle::getX() const {
	return left;
}

void Rectangle::setX(int x) {
	int dx = x - left;
	left = x;
	right += dx;
}

int Rectangle::getY() const {
	return top;
}

void Rectangle::setY(int y) {
	int dy = y - top;
	top = y;
	bottom += dy;
}

int Rectangle::getWidth() const {
	return right - left + 1;
}

void Rectangle::setWidth(int w) {
	right = left + w - 1;
}

int Rectangle::getHeight() const {
	return bottom - top + 1;
}

void Rectangle::setHeight(int h) {
	bottom = top + h - 1;
}

void Rectangle::setPosition(int x, int y) {
	setX(x);
	setY(y);
}

void Rectangle::setDimension(int w, int h) {
	setWidth(w);
	setHeight(h);
}
