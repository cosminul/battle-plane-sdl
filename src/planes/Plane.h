#ifndef PLANE_H
#define PLANE_H

#include <string>

#include "Direction.h"
#include "SceneElement.h"

class Plane: public SceneElement {
public:
	Plane(const std::string& kind, const std::string& name);

	virtual void setOrientation(Direction orientation);

	virtual void startFire();
	virtual void endFire();
	virtual void die();

private:
	Direction orientation;
};

#endif // PLANE_H
