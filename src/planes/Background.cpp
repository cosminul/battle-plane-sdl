#include "Box.h"
#include "Background.h"

Background::Background(const std::string& kind, const std::string& name):
SceneElement(kind, name) {
	box->setDimension(160, 120);
	box->setZ(0);
	speedFactor = 1;
	absoluteSpeed = 0;
}

void Background::die() {
	// Backgrounds never die :))
}
