#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <string>

class Environment
{
public:
	static std::string getResourceDir();
	static std::string getResourcePath(const std::string& resourceName);
};

#endif // ENVIRONMENT_H
