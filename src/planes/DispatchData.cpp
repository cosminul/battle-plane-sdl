#include "DispatchData.h"

DispatchData::DispatchData():
kind(),
name(),
x(0),
y(0),
direction(NO_DIRECTION)
{
}

DispatchData::DispatchData(const DispatchData& other):
kind(other.kind),
name(other.name),
x(other.x),
y(other.y),
direction(other.direction)
{
}

DispatchData::~DispatchData() {
}

DispatchData& DispatchData::operator=(const DispatchData& other) {
	if(this != &other) {
		kind.assign(other.kind);
		name.assign(other.name);
		x = other.x;
		y = other.y;
		direction = other.direction;
	}
	return *this;
}

std::string DispatchData::getKind() const {
	return kind;
}

void DispatchData::setKind(const std::string& kind) {
	this->kind.assign(kind);
}

std::string DispatchData::getName() const {
	return name;
}

void DispatchData::setName(const std::string& name) {
	this->name.assign(name);
}

int DispatchData::getX() const {
	return x;
}

void DispatchData::setX(int x) {
	this->x = x;
}

int DispatchData::getY() const {
	return y;
}

void DispatchData::setY(int y) {
	this->y = y;
}

Direction DispatchData::getDirection() const {
	return direction;
}

void DispatchData::setDirection(Direction direction) {
	this->direction = direction;
}
