#include "Box.h"
#include "Bullet.h"

const int Bullet::SIZE = 4;

Bullet::Bullet(const std::string& kind, const std::string& name):
SceneElement(kind, name) {
	box->setDimension(SIZE, SIZE);
	speedFactor = 1;
	absoluteSpeed = 3;
}

void Bullet::adjustSpeed(int reference) {
	SceneElement::adjustSpeed(reference);
	actualSpeed = speedFactor * absoluteSpeed;
}
