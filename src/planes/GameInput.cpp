#include <cstdio>

#include "GameInput.h"
#include "InputListener.h"

GameInput::GameInput():
listeners()
{
}

GameInput::~GameInput() {
	listeners.clear();
}

void GameInput::addListener(InputListener* listener) {
	listeners.push_back(listener);
}

void GameInput::startLeft() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onStartLeft();
		it++;
	}
}

void GameInput::endLeft() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onEndLeft();
		it++;
	}
}

void GameInput::startRight() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onStartRight();
		it++;
	}
}

void GameInput::endRight() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onEndRight();
		it++;
	}
}

void GameInput::startUp() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onStartUp();
		it++;
	}
}

void GameInput::endUp() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onEndUp();
		it++;
	}
}

void GameInput::startDown() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onStartDown();
		it++;
	}
}

void GameInput::endDown() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onEndDown();
		it++;
	}
}

void GameInput::startFire() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onStartFire();
		it++;
	}
}

void GameInput::endFire() {
	std::deque<InputListener*>::iterator it = listeners.begin();
	while(it != listeners.end()) {
		(*it)->onEndFire();
		it++;
	}
}
