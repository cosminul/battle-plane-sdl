#ifndef SCENEELEMENT_H
#define SCENEELEMENT_H

#include <cstdarg>
#include <string>
#include <vector>

class Box;

class SceneDispatcher;

class SceneElement {
public:
	SceneElement(const std::string& kind, const std::string& name);
	SceneElement(const SceneElement& other);
	virtual ~SceneElement();
	virtual SceneElement& operator=(const SceneElement& other);
	virtual const std::string& getKind() const;
	virtual const std::string& getName() const;
	virtual const std::string& getAppearance() const;
	virtual void setAppearance(const std::vector<std::string>& appearance);
	virtual void setDispatcher(SceneDispatcher& dispatcher);
	Box& getBox() const;

	virtual int getReferenceSpeed() const;

	/**
	 * Starts movement in the specified direction. The direction may be
	 * specified as a bitwise combination of more SceneElement::Direction
	 * values.
	 *
	 * @param direction a SceneElement::Direction value or a bitwise
	 * combination of more
	 */
	virtual void startMoving(int direction);
	/**
	 * Stops movement in the specified direction. The direction may be
	 * specified as a bitwise combination of more SceneElement::Direction
	 * values.
	 *
	 * @param direction a SceneElement::Direction value or a bitwise
	 * combination of more
	 */
	virtual void stopMoving(int direction);
	/**
	 * Stops movement in all directions.
	 */
	virtual void stopMoving();

	virtual void updatePosition();

	virtual void updateAppearance();

	virtual void adjustSpeed(int reference);

	virtual void die();

	virtual bool isAnchored() const;

	virtual void setAnchored(bool anchored);

	virtual void setVerticalSpeed(int speed);

protected:
	std::string kind;
	std::string name;
	std::vector<std::string> appearance;
	std::string currentAppearance;
	Box* box;
	SceneDispatcher* dispatcher;

	bool moveLeft;
	bool moveRight;
	bool moveUp;
	bool moveDown;
	bool anchored;
	int speedFactor;
	int absoluteSpeed;
	int actualSpeed;
	int actualSpeedY;
};

#endif // SCENEELEMENT_H
