#ifndef DISPATCHDATA_H
#define DISPATCHDATA_H

#include <string>

#include "Direction.h"

class DispatchData {
public:
	DispatchData();
	DispatchData(const DispatchData& other);
	virtual ~DispatchData();
	virtual DispatchData& operator=(const DispatchData& other);
	std::string getKind() const;
	void setKind(const std::string& kind);
	std::string getName() const;
	void setName(const std::string& name);
	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);
	Direction getDirection() const;
	/**
	 * @param direction a Direction value
	 */
	void setDirection(Direction direction);

private:
	std::string kind;
	std::string name;
	int x;
	int y;
	Direction direction;
};

#endif // DISPATCHDATA_H
