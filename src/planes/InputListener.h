#ifndef INPUTLISTENER_H
#define INPUTLISTENER_H

class InputListener {
public:
	virtual ~InputListener() {}

	virtual void onStartLeft() = 0;
	virtual void onEndLeft() = 0;
	virtual void onStartRight() = 0;
	virtual void onEndRight() = 0;
	virtual void onStartUp() = 0;
	virtual void onEndUp() = 0;
	virtual void onStartDown() = 0;
	virtual void onEndDown() = 0;
	virtual void onStartFire() = 0;
	virtual void onEndFire() = 0;
};

#endif // INPUTLISTENER_H
