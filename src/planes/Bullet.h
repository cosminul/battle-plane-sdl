#ifndef BULLET_H
#define BULLET_H

#include "SceneElement.h"

class Bullet: public SceneElement {
public:
	Bullet(const std::string& kind, const std::string& name);

	virtual void adjustSpeed(int reference);

public:
	static const int SIZE;
};

#endif // BULLET_H
