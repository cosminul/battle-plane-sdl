#ifndef RECTANGLE_H
#define RECTANGLE_H

class Rectangle {
protected:
	/** The left (x) coordinate of this Rectangle */
	int left;
	/** The top (y) coordinate of this Rectangle */
	int top;
	/** The right (x) coordinate of this Rectangle */
	int right;
	/** The bottom (y) coordinate of this Rectangle */
	int bottom;

public:
	/** Constructor */
	Rectangle();
	/**
	 * Constructor.
	 *
	 * @param x the x coordinate of this Rectangle
	 * @param y the y coordinate of this Rectangle
	 * @param w the width of this Rectangle
	 * @param h the height of this Rectangle
	 */
	Rectangle(int x, int y, int w, int h);
	/** Copy constructor */
	Rectangle(const Rectangle& other);
	/** Destructor */
	virtual ~Rectangle();
	/**
	 * Copy assignment operator.
	 *
	 * @param other another Rectangle
	 */
	virtual Rectangle& operator=(const Rectangle& other);
	/**
	 * Gets the x coordinate of this Rectangle.
	 *
	 * @return the x coordinate
	 */
	virtual int getX() const;
	/**
	 * Sets the x coordinate of this Rectangle.
	 *
	 * @param x the x coordinate to set
	 */
	virtual void setX(int x);
	/**
	 * Gets the y coordinate of this Rectangle.
	 *
	 * @return the y coordinate
	 */
	virtual int getY() const;
	/**
	 * Sets the y coordinate of this Rectangle.
	 *
	 * @param y the y coordinate to set
	 */
	virtual void setY(int y);
	/**
	 * Gets the width of this Rectangle.
	 *
	 * @return the width
	 */
	virtual int getWidth() const;
	/**
	 * Sets the width of this Rectangle.
	 *
	 * @param w the width to set
	 */
	virtual void setWidth(int w);
	/**
	 * Gets the height of this Rectangle.
	 *
	 * @return the height
	 */
	virtual int getHeight() const;
	/**
	 * Sets the height of this Rectangle.
	 *
	 * @param h the height to set
	 */
	virtual void setHeight(int h);
	virtual void setPosition(int x, int y);
	virtual void setDimension(int w, int h);
};

#endif // RECTANGLE_H
