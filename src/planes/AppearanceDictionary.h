#ifndef APPEARANCEDICTIONARY_H
#define APPEARANCEDICTIONARY_H

#include <map>
#include <string>
#include <vector>

class AppearanceDictionary {
public:
	AppearanceDictionary();
	virtual ~AppearanceDictionary();
	virtual void load();
	virtual const std::vector<std::string>& getAppearance(const std::string& actorName) const;

protected:
	/**
	 * Reads an actor appearance file.
	 *
	 * @param filename the name of the file to read
	 * @return true on success, false on failure
	 */
	virtual bool readActorAppearance(const std::string& filename);

private:
	static const int MAX_LINE_LEN;
	static const int MAX_TOKEN_LEN;
	static const std::vector<std::string> NULL_APPEARANCE;

	std::map<std::string, std::vector<std::string> > actorAppearance;
};

#endif // APPEARANCEDICTIONARY_H
