#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "AppearanceDictionary.h"
#include "BackgroundScroller.h"
#include "Box.h"
#include "Bullet.h"
#include "Environment.h"
#include "Game.h"
#include "GameInput.h"
#include "GameOutput.h"
#include "LevelRunner.h"
#include "Plane.h"
#include "Scene.h"
#include "SceneDispatcher.h"

const int Game::MAX_LINE_LEN = 512;

Game::Game():
	input(new GameInput),
	output(new GameOutput),
	state(GameState::NEW),
	bgScroll(new BackgroundScroller),
	level(new LevelRunner),
	hero(new Plane("HeroPlane", "ourHero"))
{
	input->addListener(this);
	bgScroll->setScene(output->getScene());

	hero->getBox().setPosition(5, 50);
	hero->setVerticalSpeed(4);
	output->getScene().addElement(*hero);
	output->getScene().setSpeedReference(*hero);
}

Game::~Game() {
	output->getScene().removeElement(*hero);
	delete hero;
	delete level;
	delete bgScroll;
	delete output;
	delete input;
}

GameInput& Game::getInput() const {
	return *input;
}

const GameOutput& Game::getOutput() const {
	return *output;
}

void Game::start() {
	printf("res dir=[%s]\n", Environment::getResourceDir().c_str());
	level->setSceneDispatcher(output->getSceneDispatcher());
	level->setScene(output->getScene());
	level->loadLevel(1);
	// Load appearance
	AppearanceDictionary& appearances = output->getAppearanceDictionary();
	appearances.load();

	int count = output->getScene().elementCount();
	for(int i = 0; i < count; i++) {
		SceneElement& element = output->getScene().getElementAt(i);

		const std::string& kind = element.getKind();
		element.setAppearance(appearances.getAppearance(kind));

		SceneDispatcher& dispatcher = output->getScene().getDispatcher();
		element.setDispatcher(dispatcher);
	}

	state = GameState::RUNNING;
}

void Game::update() {
	level->update();
	output->getSceneDispatcher().cleanUp();
	decideGameOver();

	int count = output->getScene().elementCount();
	// Update positions
	for(int i = 0; i < count; i++) {
		SceneElement& element = output->getScene().getElementAt(i);
		element.updatePosition();
		element.updateAppearance();
	}
	// Check for collisions
	for(int i = 0; i < count; i++) {
		for(int j = i; j < count; j++) {
			SceneElement& firstElement = output->getScene().getElementAt(i);
			SceneElement& secondElement = output->getScene().getElementAt(j);
			if(firstElement.getBox().collides(secondElement.getBox())) {
				handleCollision(firstElement, secondElement);
			}
		}
	}

	bgScroll->update();
}

GameState Game::getState() const {
	return state;
}

void Game::handleCollision(SceneElement& first, SceneElement& second) {
	printf("%s collides with %s\n", first.getName().c_str(), second.getName().c_str());
	first.die();
	second.die();
}

void Game::decideGameOver() {
	if(! output->getScene().containsElement(*hero)) {
		state = GameState::OVER;
	}
}

// InputListener implementation
void Game::onStartLeft() {
	hero->startMoving(LEFT);
}

void Game::onEndLeft() {
	hero->stopMoving(LEFT);
}

void Game::onStartRight() {
	hero->startMoving(RIGHT);
}

void Game::onEndRight() {
	hero->stopMoving(RIGHT);
}

void Game::onStartUp() {
	hero->startMoving(UP);
}

void Game::onEndUp() {
	hero->stopMoving(UP);
}

void Game::onStartDown() {
	hero->startMoving(DOWN);
}

void Game::onEndDown() {
	hero->stopMoving(DOWN);
}

void Game::onStartFire() {
	hero->startFire();
}

void Game::onEndFire() {
	hero->endFire();
}
