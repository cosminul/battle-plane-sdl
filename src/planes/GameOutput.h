#ifndef GAMEOUTPUT_H
#define GAMEOUTPUT_H

class AppearanceDictionary;
class Scene;
class SceneDispatcher;

class GameOutput {
public:
	GameOutput();
	GameOutput(const GameOutput& other);
	/**
	 * Copy assignment operator.
	 *
	 * @param other another GameOutput
	 */
	virtual GameOutput& operator=(const GameOutput& other);
	virtual ~GameOutput();
	virtual Scene& getScene() const;
	virtual SceneDispatcher& getSceneDispatcher() const;
	virtual AppearanceDictionary& getAppearanceDictionary() const;

private:
	AppearanceDictionary* appearances;
	Scene* scene;
	SceneDispatcher* sceneDispatcher;
};

#endif // GAMEOUTPUT_H
