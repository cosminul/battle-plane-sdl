#ifndef BOX_H
#define BOX_H

#include "Rectangle.h"

/**
 * A Box.
 */
class Box: public Rectangle {
public:
	/** Constructor */
	Box();
	/**
	 * Creates a new box with a default z coordinate of 1.
	 *
	 * @param x the x coordinate of this box
	 * @param y the y coordinate of this box
	 * @param w the width of this box
	 * @param h the height of this box
	 */
	Box(int x, int y, int w, int h);
	/**
	 * Creates a new box with the specified coordinates and size.
	 *
	 * @param x the x coordinate of this box
	 * @param y the y coordinate of this box
	 * @param z the z coordinate of this box
	 * @param w the width of this box
	 * @param h the height of this box
	 */
	Box(int x, int y, int z, int w, int h);
	/**
	 * Gets the z coordinate of this Box.
	 *
	 * @return the z coordinate
	 */
	virtual int getZ() const;
	/**
	 * Sets the z coordinate of this Box.
	 *
	 * @param z the z coordinate to set
	 */
	virtual void setZ(int z);
	/**
	 * Moves this box with the specified number of units to the right.
	 *
	 * @param units the number of units to move to the right. A negative
	 * value moves to the left.
	 */
	void moveX(int units);
	/**
	 * Moves this box with the specified number of units down.
	 *
	 * @param units the number of units to move down. A negative value
	 * moves up.
	 */
	void moveY(int units);
	/**
	 * Tells whether this box touches another box. Two boxes touch if
	 * they collide, or if one side of a box and another side of the
	 * other box are adjacent.
	 *
	 * @param other the other box to test touch with
	 * @return true if this box touches the other box; false otherwise
	 */
	bool touches(const Box& other) const;
	/**
	 * Tells whether this box collides with another box. Two boxes collide
	 * if two respective sides of the boxes overlap, or if one side
	 * of a box is inside the other box.
	 *
	 * @param other the other box to test collision with
	 * @return true if this box collides with the other box; false otherwise
	 */
	bool collides(const Box& other) const;
	/**
	 * Tells whether this box would collide with another box if they were
	 * in the same plane.
	 *
	 * @param other the other box to test collision with
	 * @return true if this box collides with the other box; false otherwise
	 * @see collides(const Box&)
	 */
	bool collidesInAnyPlane(const Box& other) const;

private:
	bool collides(const Box& other, int minimumDistance) const;
	bool collidesInAnyPlane(const Box& other, int minimumDistance) const;

protected:
	/** The z coordinate of this Box */
	int z;
};

#endif // BOX_H
