#include <cstdio>

#include "AppearanceDictionary.h"
#include "Box.h"
#include "Bullet.h"
#include "DispatchData.h"
#include "Plane.h"
#include "Scene.h"
#include "SceneDispatcher.h"
#include "Ufo.h"

SceneDispatcher::SceneDispatcher():
appearances(NULL),
scene(NULL),
ownedElements()
{
}

SceneDispatcher::SceneDispatcher(const SceneDispatcher& other):
appearances(other.appearances),
scene(other.scene),
ownedElements(other.ownedElements)
{
}

SceneDispatcher& SceneDispatcher::operator=(const SceneDispatcher& other) {
	if(this != &other) {
		appearances = other.appearances;
		scene = other.scene;
		ownedElements = other.ownedElements;
	}
	return *this;
}

SceneDispatcher::~SceneDispatcher() {
	int count = scene->elementCount();
	// Iterate in reverse order, so we can safely delete by index
	for(int i = count - 1; i >= 0; i--) {
		SceneElement& element = scene->getElementAt(i);
		removeSceneElement(element);
	}
}

void SceneDispatcher::setScene(Scene& scene) {
	this->scene = &scene;
}

void SceneDispatcher::setAppearanceDictionary(AppearanceDictionary &appearances) {
	this->appearances = &appearances;
}

Bullet& SceneDispatcher::dispatchNewBullet(const DispatchData& data) {
	Bullet* bullet = new Bullet(data.getKind(), data.getName());
	dispatchSceneElement(*bullet, data);
	return *bullet;
}

Plane& SceneDispatcher::dispatchNewPlane(const DispatchData& data) {
	Plane* plane = new Plane(data.getKind(), data.getName());
	plane->setOrientation(data.getDirection());
	dispatchSceneElement(*plane, data);
	return *plane;
}

Ufo& SceneDispatcher::dispatchNewUfo(const DispatchData& data) {
	Ufo* ufo = new Ufo(data.getKind(), data.getName());
	dispatchSceneElement(*ufo, data);
	return *ufo;
}

void SceneDispatcher::cleanUp() {
	int count = scene->elementCount();
	// Iterate in reverse order, so we can safely delete by index
	for(int i = count - 1; i >= 0; i--) {
		SceneElement& element = scene->getElementAt(i);
		// If the element is anchored
		if(element.isAnchored()) {
			// Don't attempt to remove it from the scene
			continue;
		}
		// If the element is outside the scene
		if(! element.getBox().collidesInAnyPlane(scene->getBox())) {
			removeSceneElement(element);
		}
	}
}

void SceneDispatcher::dispatchSceneElement(SceneElement &element, const DispatchData& data) {
	element.setDispatcher(*this);
	element.setAppearance(appearances->getAppearance(data.getKind()));
	ownedElements.insert(&element);
	element.getBox().setPosition(data.getX(), data.getY());
	element.startMoving(data.getDirection());
	if(scene == NULL) {
		printf("scene is null!\n");
	}
	scene->addElement(element);
}

void SceneDispatcher::removeSceneElement(SceneElement& element) {
	// Remove it from the scene
	printf("removing %s from scene\n", element.getName().c_str());
	scene->removeElement(element);
	// Release memory only if we really own it
	if(ownedElements.find(&element) != ownedElements.end()) {
		printf("cleaning up %s\n", element.getName().c_str());
		ownedElements.erase(&element);
		delete &element;
	}
}
