#ifndef GAMESTATE_H
#define GAMESTATE_H

enum GameState {
	NEW,
	RUNNING,
	PAUSED,
	OVER
};

#endif // GAMESTATE_H
