#include <cstdio>
#include <cstring>

#include "Environment.h"
#include "FileUtil.h"
#include "LevelInstant.h"
#include "LevelRunner.h"
#include "PlaneInstantHandler.h"
#include "StringUtil.h"
#include "UfoInstantHandler.h"

const int LevelRunner::MAX_LINE_LEN = 512;
const int LevelRunner::MAX_TOKEN_LEN = 128;

LevelRunner::LevelRunner():
instant(new LevelInstant),
available(false),
in(NULL),
time(0L),
planeInstantHandler(new PlaneInstantHandler),
ufoInstantHandler(new UfoInstantHandler)
{
}

LevelRunner::LevelRunner(const LevelRunner& other):
instant(new LevelInstant(*other.instant)),
available(other.available),
in(other.in),
time(other.time),
planeInstantHandler(other.planeInstantHandler->clone()),
ufoInstantHandler(other.ufoInstantHandler->clone())
{
}

LevelRunner& LevelRunner::operator=(const LevelRunner& other) {
	if(this != &other) {
	}
	return *this;
}

LevelRunner::~LevelRunner() {
	if(in != NULL) {
		fclose(in);
		in = NULL;
	}
	delete instant;
	delete planeInstantHandler;
	delete ufoInstantHandler;
}

void LevelRunner::loadLevel(int levelNumber) {
	std::string filename = Environment::getResourcePath("level");
	char lvl[MAX_LINE_LEN];
	sprintf(lvl, "%s%d.%s", filename.c_str(), levelNumber, "txt");
	printf("loading level %s\n", lvl);
	if(in != NULL) {
		fclose(in);
	}
	in = fopen(lvl, "r");
}

void LevelRunner::update() {
	const LevelInstant* inst = getInstant();
	if(inst != NULL) {
		printf("%s\n", instant->toString().c_str());
		handleInstant(instant);
	}
	time++;
}

void LevelRunner::setSceneDispatcher(SceneDispatcher& dispatcher) {
	planeInstantHandler->setDispatcher(dispatcher);
	ufoInstantHandler->setDispatcher(dispatcher);
}

void LevelRunner::setScene(Scene& scene) {
	planeInstantHandler->setScene(scene);
	ufoInstantHandler->setScene(scene);
}

const LevelInstant* LevelRunner::getInstant() {
	if(available == false) {
		char line[MAX_LINE_LEN]; // the buffer for the entire line
		if(my_fline(in, line, MAX_LINE_LEN)) {
			readInstantFromLine(line);
			available = true;
		}
	}
	if(available == true) {
		if(instant->getTime() <= time) {
			available = false;
			return instant;
		}
	}
	return NULL;
}

void LevelRunner::readInstantFromLine(char* line) {
	char token[MAX_TOKEN_LEN];
	char* pch = strtok(line, ",");
	int index = 0;
	instant->clearParams();
	while(pch != NULL) {
		my_strim(pch, strlen(pch), token, MAX_TOKEN_LEN);
		switch(index) {
		case 0:
			instant->setTime(token);
			break;
		case 1:
			instant->setActor(token);
			break;
		case 2:
			instant->setAction(token);
			break;
		default:
			instant->addParam(token);
			break;
		}
		pch = strtok(NULL, ",");
		index++;
	}
}

void LevelRunner::handleInstant(LevelInstant *instant) {
	const std::string& varName = instant->getActor();
	InstantHandler* instantHandler = NULL;
	if(varName.substr(0, strlen("plane-")) == "plane-") {
		instantHandler = planeInstantHandler;
	} else if(varName.substr(0, strlen("ufo-")) == "ufo-") {
		instantHandler = ufoInstantHandler;
	}
	instantHandler->handleInstant(*instant);
}
