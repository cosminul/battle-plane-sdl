#ifndef PLANEINSTANTHANDLER_H
#define PLANEINSTANTHANDLER_H

#include <map>
#include <string>

#include "InstantHandler.h"

class LevelInstant;
class Plane;

class PlaneInstantHandler: public InstantHandler {
public:
	PlaneInstantHandler();
	PlaneInstantHandler(const PlaneInstantHandler& other);
	/**
	 * Virtual copy constructor
	 */
	virtual PlaneInstantHandler* clone() const;
	virtual PlaneInstantHandler& operator=(const PlaneInstantHandler& other);
	virtual ~PlaneInstantHandler();
	virtual void handleInstant(const LevelInstant& instant);

private:
	Plane* handleCreatePlane(const LevelInstant& instant);

private:
	std::map<std::string, Plane*> planes;
};

#endif // PLANEINSTANTHANDLER_H
