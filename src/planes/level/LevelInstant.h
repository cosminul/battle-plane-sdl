#ifndef LEVELINSTANT_H
#define LEVELINSTANT_H

#include <string>
#include <vector>

class LevelInstant {
public:
	LevelInstant();
	long getTime() const;
	void setTime(long time);
	void setTime(const std::string& time);
	const std::string& getActor() const;
	void setActor(const std::string& actor);
	const std::string& getAction() const;
	void setAction(const std::string& action);
	const std::vector<std::string>& getParams() const;
	void addParam(const std::string& param);
	void clearParams();
	std::string toString() const;

private:
	long time;
	std::string actor;
	std::string action;
	std::vector<std::string> params;
};

#endif // LEVELINSTANT_H
