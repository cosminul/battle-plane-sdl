#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "LevelInstant.h"

LevelInstant::LevelInstant():
time(0L),
actor(),
action(),
params()
{
}

long LevelInstant::getTime() const {
	return time;
}

void LevelInstant::setTime(long time) {
	this->time = time;
}

void LevelInstant::setTime(const std::string& time) {
	this->time = atoi(time.c_str());
}

const std::string& LevelInstant::getActor() const {
	return actor;
}

void LevelInstant::setActor(const std::string& actor) {
	this->actor.assign(actor);
}

const std::string& LevelInstant::getAction() const {
	return action;
}

void LevelInstant::setAction(const std::string& action) {
	this->action.assign(action);
}

const std::vector<std::string>& LevelInstant::getParams() const {
	return params;
}

void LevelInstant::addParam(const std::string& param) {
	params.push_back(param);
}

void LevelInstant::clearParams() {
	params.clear();
}

std::string LevelInstant::toString() const {
	char s[128];
	sprintf(s, "%ld %s %s", time, actor.c_str(), action.c_str());
	for(unsigned int i = 0; i < params.size(); i++) {
		strcat(s, " ");
		strcat(s, params[i].c_str());
	}
	return s;
}
