#ifndef UFOINSTANTHANDLER_H
#define UFOINSTANTHANDLER_H

#include <map>
#include <string>

#include "InstantHandler.h"

class LevelInstant;
class Ufo;

class UfoInstantHandler: public InstantHandler {
public:
	UfoInstantHandler();
	UfoInstantHandler(const UfoInstantHandler& other);
	/**
	 * Virtual copy constructor
	 */
	virtual UfoInstantHandler* clone() const;
	virtual UfoInstantHandler& operator=(const UfoInstantHandler& other);
	virtual ~UfoInstantHandler();
	virtual void handleInstant(const LevelInstant& instant);

private:
	Ufo* handleCreateUfo(const LevelInstant& instant);

private:
	std::map<std::string, Ufo*> ufos;
};

#endif // UFOINSTANTHANDLER_H
