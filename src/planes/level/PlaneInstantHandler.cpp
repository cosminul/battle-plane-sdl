#include <cstdio>

#include "DispatchData.h"
#include "LevelInstant.h"
#include "Plane.h"
#include "PlaneInstantHandler.h"
#include "Scene.h"
#include "SceneDispatcher.h"

PlaneInstantHandler::PlaneInstantHandler():
InstantHandler(),
planes()
{
}

PlaneInstantHandler::PlaneInstantHandler(const PlaneInstantHandler& other):
InstantHandler(other),
planes(other.planes)
{
}

PlaneInstantHandler* PlaneInstantHandler::clone() const
{
	return new PlaneInstantHandler(*this);
}

PlaneInstantHandler& PlaneInstantHandler::operator=(const PlaneInstantHandler& other)
{
	if(this != &other) {
		planes = other.planes;
	}
	return *this;
}

PlaneInstantHandler::~PlaneInstantHandler() {
}

void PlaneInstantHandler::handleInstant(const LevelInstant& instant) {
	const std::string& varName = instant.getActor();
	const std::string& function = instant.getAction();
	std::map<std::string, Plane*>::iterator it = planes.find(varName);
	if(it != planes.end()) {
		Plane& plane = *(it->second);
		printf("found %s\n", varName.c_str());
		if(!scene->containsElement(plane)) {
			printf("%s not in scene\n", varName.c_str());
			return;
		}
		if(function == "fire") {
			printf("fire!\n");
			it->second->startFire();
		}
		handleMovementInstant(instant, plane);
	} else {
		printf("not found %s\n", varName.c_str());
		if(function == "create-plane") {
			printf("create plane %s\n", varName.c_str());
			Plane* plane = handleCreatePlane(instant);
			planes.insert(std::pair<std::string, Plane*>(varName, plane));
		} else {
			printf("unknown variable %s\n", varName.c_str());
		}
	}
}

Plane* PlaneInstantHandler::handleCreatePlane(const LevelInstant& instant) {
	DispatchData data = extractDispatchData(instant);
	Plane& plane = dispatcher->dispatchNewPlane(data);
	return &plane;
}
