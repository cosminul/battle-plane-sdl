#ifndef LEVELRUNNER_H
#define LEVELRUNNER_H

#include <cstdio>

class InstantHandler;
class LevelInstant;
class Scene;
class SceneDispatcher;

class LevelRunner {
public:
	LevelRunner();
	LevelRunner(const LevelRunner& other);
	virtual LevelRunner& operator=(const LevelRunner& other);
	virtual ~LevelRunner();
	void loadLevel(int levelNumber);
	void update();
	void setSceneDispatcher(SceneDispatcher& dispatcher);
	void setScene(Scene& scene);

private:
	const LevelInstant* getInstant();
	void readInstantFromLine(char* line);
	void handleInstant(LevelInstant* instant);

private:
	static const int MAX_LINE_LEN;
	static const int MAX_TOKEN_LEN;

	LevelInstant* instant;
	bool available;
	FILE* in;
	long time;
	InstantHandler* planeInstantHandler;
	InstantHandler* ufoInstantHandler;
};

#endif // LEVELRUNNER_H
