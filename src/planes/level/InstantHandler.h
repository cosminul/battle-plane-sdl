#ifndef INSTANTHANDLER_H
#define INSTANTHANDLER_H

#include "DispatchData.h"

class LevelInstant;
class Scene;
class SceneElement;
class SceneDispatcher;

class InstantHandler {
public:
	InstantHandler();
	InstantHandler(const InstantHandler& other);
	/**
	 * Virtual copy constructor
	 */
	virtual InstantHandler* clone() const = 0;
	virtual InstantHandler& operator=(const InstantHandler& other);
	virtual ~InstantHandler();
	virtual void setScene(Scene& scene);
	virtual void setDispatcher(SceneDispatcher& dispatcher);
	virtual void handleInstant(const LevelInstant& instant) = 0;

protected:
	void handleMovementInstant(const LevelInstant& instant, SceneElement& element);
	DispatchData extractDispatchData(const LevelInstant& instant);

protected:
	Scene* scene;
	SceneDispatcher* dispatcher;
};

#endif // INSTANTHANDLER_H
