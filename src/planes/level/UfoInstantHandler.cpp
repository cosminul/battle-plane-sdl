#include <cstdio>

#include "DispatchData.h"
#include "LevelInstant.h"
#include "Scene.h"
#include "SceneDispatcher.h"
#include "Ufo.h"
#include "UfoInstantHandler.h"

UfoInstantHandler::UfoInstantHandler():
InstantHandler(),
ufos()
{
}

UfoInstantHandler::UfoInstantHandler(const UfoInstantHandler& other):
InstantHandler(other),
ufos(other.ufos)
{
}

UfoInstantHandler* UfoInstantHandler::clone() const
{
	return new UfoInstantHandler(*this);
}

UfoInstantHandler& UfoInstantHandler::operator=(const UfoInstantHandler& other)
{
	if(this != &other) {
		ufos = other.ufos;
	}
	return *this;
}

UfoInstantHandler::~UfoInstantHandler() {
}

void UfoInstantHandler::handleInstant(const LevelInstant& instant) {
	const std::string& varName = instant.getActor();
	const std::string& function = instant.getAction();
	std::map<std::string, Ufo*>::iterator it = ufos.find(varName);
	if(it != ufos.end()) {
		Ufo& ufo = *(it->second);
		printf("found %s\n", varName.c_str());
		if(!scene->containsElement(ufo)) {
			printf("%s not in scene\n", varName.c_str());
			return;
		}
		handleMovementInstant(instant, ufo);
	} else {
		printf("not found %s\n", varName.c_str());
		if(function == "create-ufo") {
			printf("create ufo %s\n", varName.c_str());
			Ufo* ufo = handleCreateUfo(instant);
			ufos.insert(std::pair<std::string, Ufo*>(varName, ufo));
		} else {
			printf("unknown variable %s\n", varName.c_str());
		}
	}
}

Ufo* UfoInstantHandler::handleCreateUfo(const LevelInstant& instant) {
	DispatchData data = extractDispatchData(instant);
	Ufo& ufo = dispatcher->dispatchNewUfo(data);
	return &ufo;
}
