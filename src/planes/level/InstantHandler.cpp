#include <cstdlib>

#include "InstantHandler.h"
#include "LevelInstant.h"
#include "SceneElement.h"

InstantHandler::InstantHandler():
scene(NULL),
dispatcher(NULL)
{
}

InstantHandler::InstantHandler(const InstantHandler& other):
scene(other.scene),
dispatcher(other.dispatcher)
{
}

InstantHandler& InstantHandler::operator=(const InstantHandler& other) {
	if(this != &other) {
		scene = other.scene;
		dispatcher = other.dispatcher;
	}
	return *this;
}

InstantHandler::~InstantHandler() {
}

void InstantHandler::setScene(Scene& scene) {
	this->scene = &scene;
}

void InstantHandler::setDispatcher(SceneDispatcher& dispatcher) {
	this->dispatcher = &dispatcher;
}

void InstantHandler::handleMovementInstant(const LevelInstant& instant, SceneElement& element) {
	const std::string& function = instant.getAction();
	if(function == "start-up") {
		element.startMoving(UP);
	} else if(function == "stop-up") {
		element.stopMoving(UP);
	} else if(function == "start-down") {
		element.startMoving(DOWN);
	} else if(function == "stop-down") {
		element.stopMoving(DOWN);
	}
}

DispatchData InstantHandler::extractDispatchData(const LevelInstant& instant) {
	DispatchData data;

	const std::string& kind = instant.getParams().at(0);
	data.setKind(kind);
	const std::string& name = instant.getActor();
	data.setName(name);
	int x = atoi(instant.getParams().at(1).c_str());
	data.setX(x);
	int y = atoi(instant.getParams().at(2).c_str());
	data.setY(y);
	const std::string& dir = instant.getParams().at(3);
	Direction direction = NO_DIRECTION;
	if(dir == "left") {
		direction = LEFT;
	} else if(dir == "right") {
		direction = RIGHT;
	}
	data.setDirection(direction);

	return data;
}
