#include "AppearanceDictionary.h"
#include "GameOutput.h"
#include "Scene.h"
#include "SceneDispatcher.h"

GameOutput::GameOutput():
appearances(new AppearanceDictionary),
scene(new Scene),
sceneDispatcher(new SceneDispatcher)
{
	scene->setDispatcher(*sceneDispatcher);
	sceneDispatcher->setScene(*scene);
	sceneDispatcher->setAppearanceDictionary(*appearances);
}

GameOutput::GameOutput(const GameOutput& other):
appearances(new AppearanceDictionary(*other.appearances)),
scene(new Scene(*other.scene)),
sceneDispatcher(new SceneDispatcher(*other.sceneDispatcher))
{
}

GameOutput& GameOutput::operator=(const GameOutput& other) {
	if(this != &other) {
		delete appearances;
		appearances = new AppearanceDictionary(*other.appearances);
		delete scene;
		scene = new Scene(*other.scene);
		delete sceneDispatcher;
		sceneDispatcher = new SceneDispatcher(*other.sceneDispatcher);
	}
	return *this;
}

GameOutput::~GameOutput() {
	delete sceneDispatcher;
	delete scene;
	delete appearances;
}

Scene& GameOutput::getScene() const {
	return *scene;
}

SceneDispatcher& GameOutput::getSceneDispatcher() const {
	return *sceneDispatcher;
}

AppearanceDictionary& GameOutput::getAppearanceDictionary() const {
	return *appearances;
}
