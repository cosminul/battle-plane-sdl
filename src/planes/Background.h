#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "SceneElement.h"

class Background: public SceneElement {
public:
	Background(const std::string& kind, const std::string& name);
	virtual void die();
};

#endif // BACKGROUND_H
