#include "Box.h"
#include "Bullet.h"
#include "DispatchData.h"
#include "SceneDispatcher.h"
#include "Plane.h"

Plane::Plane(const std::string& kind, const std::string& name):
SceneElement(kind, name),
orientation(RIGHT)
{
	box->setDimension(16, 20);
	speedFactor = 1;
	absoluteSpeed = 1;
	actualSpeedY = 2;
}

void Plane::setOrientation(Direction orientation) {
	this->orientation = orientation;
}

void Plane::startFire() {
	std::string bulletKind = "RBullet";
	int x = box->getX() + box->getWidth();
	if(orientation == LEFT) {
		bulletKind = "LBullet";
		x = box->getX() - Bullet::SIZE;
	}
	int y = box->getY() + (box->getHeight() / 2) - 2;

	DispatchData data;
	data.setKind(bulletKind);
	data.setName("bullet");
	data.setX(x);
	data.setY(y);
	data.setDirection(orientation);
	dispatcher->dispatchNewBullet(data);
}

void Plane::endFire() {
}

void Plane::die() {
	// TODO implement some flickering or explosion before actually disappearing
	SceneElement::die();
}
