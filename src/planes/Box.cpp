#include "Box.h"

Box::Box():
Rectangle(),
z(1)
{
}

Box::Box(int x, int y, int w, int h):
Rectangle(x, y, w, h),
z(1)
{
}

Box::Box(int x, int y, int z, int w, int h):
Rectangle(x, y, w, h),
z(z)
{
}

int Box::getZ() const {
	return z;
}

void Box::setZ(int z) {
	this->z = z;
}

void Box::moveX(int units) {
	left += units;
	right += units;
}

void Box::moveY(int units) {
	top += units;
	bottom += units;
}

bool Box::touches(const Box& other) const {
	return collides(other, 1);
}

bool Box::collides(const Box& other) const {
	return collides(other, 0);
}

bool Box::collidesInAnyPlane(const Box& other) const {
	return collidesInAnyPlane(other, 0);
}

bool Box::collides(const Box& other, int minimumDistance) const {
	if(z != other.z) {
		// Do not collide if in different planes
		return false;
	}
	return collidesInAnyPlane(other, minimumDistance);
}

bool Box::collidesInAnyPlane(const Box& other, int minimumDistance) const {
	if(this == &other) {
		// Cannot collide with self
		return false;
	}
	// If any of the sides from this are outside of other
	if(bottom + minimumDistance < other.top) {
		return false;
	}
	if(top - minimumDistance > other.bottom) {
		return false;
	}
	if(right + minimumDistance < other.left) {
		return false;
	}
	if(left - minimumDistance > other.right) {
		return false;
	}
	// If none of the sides from this are outside other
	return true;
}
