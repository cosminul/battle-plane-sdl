#include "Box.h"
#include "Scene.h"
#include "SceneDispatcher.h"
#include "SceneElement.h"

const int Scene::WIDTH = 160;
const int Scene::HEIGHT = 120;

Scene::Scene():
elements(),
dispatcher(NULL),
box(new Box(0, 0, WIDTH, HEIGHT)),
speedReference(NULL)
{
}

Scene::Scene(const Scene &other):
elements(other.elements),
dispatcher(other.dispatcher),
box(new Box(*other.box)),
speedReference(other.speedReference)
{
}

Scene::~Scene() {
	elements.clear();
	delete box;
}

Scene& Scene::operator=(const Scene& other) {
	if(this != &other) {
		dispatcher = other.dispatcher;
		delete box;
		box = new Box(*other.box);
		speedReference = other.speedReference;
	}
	return *this;
}

void Scene::setDispatcher(SceneDispatcher& dispatcher) {
	this->dispatcher = &dispatcher;
}

void Scene::addElement(SceneElement& element) {
	if(speedReference != NULL) {
		element.adjustSpeed(speedReference->getReferenceSpeed());
	}
	elements.push_back(&element);
}

int Scene::elementCount() const {
	return elements.size();
}

SceneElement& Scene::getElementAt(int index) const {
	return *elements[index];
}

void Scene::removeElementAt(int index) {
	elements.erase(elements.begin() + index);
}

void Scene::removeElement(SceneElement& element) {
	std::deque<SceneElement*>::iterator it = elements.begin();
	while(it != elements.end()) {
		if(*it == &element) {
			elements.erase(it);
			return;
		}
		it++;
	}
}

bool Scene::containsElement(SceneElement& element) const {
	std::deque<SceneElement*>::const_iterator cit = elements.begin();
	while(cit != elements.end()) {
		if(*cit == &element) {
			return true;
		}
		cit++;
	}
	return false;
}

SceneDispatcher& Scene::getDispatcher() const {
	return *dispatcher;
}

Box& Scene::getBox() const {
	return *box;
}

void Scene::adjustSpeeds() {
	std::deque<SceneElement*>::iterator it = elements.begin();
	while(it != elements.end()) {
		(*it)->adjustSpeed(speedReference->getReferenceSpeed());
		it++;
	}
}

void Scene::setSpeedReference(SceneElement& reference) {
	speedReference = &reference;
	adjustSpeeds();
}
