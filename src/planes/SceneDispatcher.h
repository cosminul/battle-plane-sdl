#ifndef SCENEDISPATCHER_H
#define SCENEDISPATCHER_H

#include <set>
#include <string>

#include "Direction.h"

class AppearanceDictionary;
class Bullet;
class DispatchData;
class Plane;
class Scene;
class SceneElement;
class Ufo;

class SceneDispatcher {
public:
	SceneDispatcher();
	SceneDispatcher(const SceneDispatcher& other);
	/**
	 * Copy assignment operator.
	 *
	 * @param other another SceneDispatcher
	 */
	virtual SceneDispatcher& operator=(const SceneDispatcher& other);
	virtual ~SceneDispatcher();
	virtual void setScene(Scene& scene);
	virtual void setAppearanceDictionary(AppearanceDictionary& appearances);
	/**
	 * Dispatches a new bullet.
	 * @param data the dispatch data to be used
	 * @return the dispatched bullet
	 */
	virtual Bullet& dispatchNewBullet(const DispatchData& data);
	/**
	 * Dispatches a new plane.
	 * @param data the dispatch data to be used
	 * @return the dispatched plane
	 */
	virtual Plane& dispatchNewPlane(const DispatchData& data);
	/**
	 * Dispatches a new UFO.
	 * @param data the dispatch data to be used
	 * @return the dispatched UFO
	 */
	virtual Ufo& dispatchNewUfo(const DispatchData& data);
	virtual void cleanUp();

private:
	/**
	 * Dispatches a scene element.
	 * @param data the dispatch data to be used
	 */
	void dispatchSceneElement(SceneElement& element, const DispatchData& data);
	void removeSceneElement(SceneElement& element);

private:
	AppearanceDictionary* appearances;
	Scene* scene;
	std::set<SceneElement*> ownedElements;
};

#endif // SCENEDISPATCHER_H
