#include "Environment.h"

std::string Environment::getResourceDir() {
	return RESOURCE_DIR; // defined at compile time, in the build system
}

std::string Environment::getResourcePath(const std::string& resourceName) {
	std::string resourcePath = RESOURCE_DIR;
	resourcePath += "/" + resourceName;
	return resourcePath;
}
