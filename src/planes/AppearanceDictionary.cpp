#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "AppearanceDictionary.h"
#include "Environment.h"
#include "FileUtil.h"
#include "StringUtil.h"

const int AppearanceDictionary::MAX_LINE_LEN = 512;
const int AppearanceDictionary::MAX_TOKEN_LEN = 128;
const std::vector<std::string> AppearanceDictionary::NULL_APPEARANCE;

AppearanceDictionary::AppearanceDictionary():
actorAppearance()
{
}

AppearanceDictionary::~AppearanceDictionary() {
}

void AppearanceDictionary::load() {
	std::string filename = Environment::getResourcePath("appearance.txt");
	readActorAppearance(filename);
}

bool AppearanceDictionary::readActorAppearance(const std::string& filename) {
	FILE* in = fopen(filename.c_str(), "r");
	char line[MAX_LINE_LEN]; // the buffer for the entire line
	char name[MAX_TOKEN_LEN];
	char value[MAX_TOKEN_LEN];
	// position of the equal sign (key/value separator)
	char* eqPos = NULL;
	char* tok1 = NULL;
	int tok1_len = 0;
	char* tok2 = NULL;
	int tok2_len = 0;

	while(my_fline(in, line, MAX_LINE_LEN)) {
		// If the line contains 2 tokens separated by equal sign
		eqPos = strchr(line, '=');
		if(eqPos == NULL) {
			continue;
		}
		tok1 = line;
		tok1_len = eqPos - line;
		tok2 = eqPos + 1;
		tok2_len = strlen(line) - tok1_len - 1;
		my_strim(tok1, tok1_len, name, MAX_TOKEN_LEN);
		my_strim(tok2, tok2_len, value, MAX_TOKEN_LEN);

		std::vector<std::string> spriteNames;
		char spriteName[MAX_TOKEN_LEN];
		printf("search for appearance in %s\n", value);
		char* pch = strtok(value, ",");
		while(pch != NULL) {
			my_strim(pch, strlen(pch), spriteName, MAX_TOKEN_LEN);
			spriteNames.push_back(spriteName);
			printf("found appearance %s\n", spriteName);
			pch = strtok(NULL, ",");
		}

		std::pair<std::string, std::vector<std::string> > attrs;
		printf("setting %lu appearances for %s\n", spriteNames.size(), name);
		attrs.first = name;
		attrs.second = spriteNames;
		actorAppearance.insert(attrs);
	}
	fclose(in);
	return true;
}

const std::vector<std::string>& AppearanceDictionary::getAppearance(const std::string& actorKind) const {
	std::map<std::string, std::vector<std::string> >::const_iterator cit;
	cit = actorAppearance.find(actorKind);
	if(cit == actorAppearance.end()) {
		printf("actor %s not found\n", actorKind.c_str());
		return NULL_APPEARANCE;
	}
	const std::vector<std::string>& names = cit->second;
	return names;
}
