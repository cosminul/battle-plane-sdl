#include <cstdio>

#include "Box.h"
#include "SceneDispatcher.h"
#include "Ufo.h"

const int Ufo::UPDATES_BETWEEN_APPEARANCE_CHANGES = 30;

Ufo::Ufo(const std::string& kind, const std::string& name):
SceneElement(kind, name),
appearanceIndex(0),
updatesSinceLastChange(0)
{
	box->setDimension(16, 8);
	speedFactor = 1;
	absoluteSpeed = 1;
}

void Ufo::updateAppearance() {
	updatesSinceLastChange++;
	if(updatesSinceLastChange >= UPDATES_BETWEEN_APPEARANCE_CHANGES) {
		appearanceIndex++;
		appearanceIndex = appearanceIndex % appearance.size();
		updatesSinceLastChange = 0;
	}

	currentAppearance = appearance[appearanceIndex];
}

void Ufo::die() {
	// TODO implement some flickering or explosion before actually disappearing
	SceneElement::die();
}
