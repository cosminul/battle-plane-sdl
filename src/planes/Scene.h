#ifndef SCENE_H
#define SCENE_H

#include <deque>

class Box;
class SceneDispatcher;
class SceneElement;

class Scene {
public:
	Scene();
	Scene(const Scene& other);
	virtual ~Scene();
	virtual Scene& operator=(const Scene& other);

	void setDispatcher(SceneDispatcher& dispatcher);
	void addElement(SceneElement& element);
	int elementCount() const;
	SceneElement& getElementAt(int index) const;
	void removeElementAt(int index);
	void removeElement(SceneElement& element);
	bool containsElement(SceneElement& element) const;
	SceneDispatcher& getDispatcher() const;
	Box& getBox() const;
	void setSpeedReference(SceneElement& reference);

public:
	/** The scene width, in world units */
	static const int WIDTH;
	/** The scene height, in world units */
	static const int HEIGHT;

private:
	void adjustSpeeds();

private:
	std::deque<SceneElement*> elements;
	SceneDispatcher* dispatcher;
	Box* box;
	SceneElement* speedReference;
};

#endif // SCENE_H
