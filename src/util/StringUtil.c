/*
 * string_util.c
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <string.h>

#include "StringUtil.h"

int min(int a, int b) {
	return (a < b) ? a : b;
}

size_t my_strim(const char* in_str, size_t in_len, char *out_str, size_t out_len) {
	const char* original_in_str = in_str;
	const char *end = NULL;
	size_t out_size = 0;

	if(out_len == 0) {
		*out_str = 0;
		return 0;
	}

	/* Trim leading space */
	while(isspace(*in_str)) {
		in_str++;
	}

	if(*in_str == 0) {
		/* The input string is all spaces */
		*out_str = 0;
		return 0;
	}

	/* Trim trailing space */
	end = original_in_str + in_len - 1;
	while(end > in_str && isspace(*end)) {
		end--;
	}
	end++;

	/* Set output size to minimum of
	 * trimmed string length and buffer size minus 1
	 */
	out_size = min((end - in_str), (out_len - 1));

	/* Copy trimmed string and add null terminator */
	memcpy(out_str, in_str, out_size);
	out_str[out_size] = 0;

	return out_size;
}
