/*
 * file_util.c
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "FileUtil.h"

int my_fline(FILE* stream, char* line_buffer, size_t size) {
	char* line = malloc(size * sizeof(char) + 1);
	int code = 0;
	unsigned char c = '\0';
	unsigned int i = 0;
	while((code = fgetc(stream)) != EOF) {
		c = (unsigned char)code;
		if((c == '\n' || c == '\r') && (i > 0)) {
			line[i] = '\0';
			strncpy(line_buffer, line, i + 1);
			free(line);
			i = 0;
			return 1;
		}
		if((c != '\n' && c != '\r') && (i < size)) {
			line[i] = c;
			i++;
		}
	}
	if(i > 0) {
		line[i] = '\0';
		strncpy(line_buffer, line, i + 1);
		free(line);
		return 1;
	}
	free(line);
	return 0;
}
