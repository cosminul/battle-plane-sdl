/*
 * lapse.h
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file lapse.h
 */
#ifndef LAPSE_H
#define LAPSE_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * A <code>properties_t</code> contains a set of (key, value) pairs.
 * Memory management of <code>properties_t</code> is done with properties_reference() and properties_destroy().
 */
typedef struct _properties properties_t;

/**
 * Creates a new ::properties_t containing no properties.
 *
 * @return a newly allocated ::properties_t with a reference count of 1.
 */
properties_t* properties_create();

/**
 * Increases the reference count on <code>prop</code> by one.
 * This prevents <code>prop</code> from being destroyed
 * until a matching call to properties_destroy() is made.
 *
 * @param prop a ::properties_t
 * @return the referenced ::properties_t
 */
properties_t* properties_reference(properties_t* prop);

/**
 * Decreases the reference count on <code>prop</code> by one.
 * If the result is zero, then <code>prop</code> and all associated resources are freed.
 * See properties_reference().
 *
 * @param prop a ::properties_t
 */
void properties_destroy(properties_t* prop);

/**
 * Sets the value of a property in a ::properties_t, specified by its key.
 * If a property with the same key already exists in <code>prop</code>,
 * then its value is replaced.
 * Otherwise, a new property is created and added to <code>prop</code>.
 *
 * @param prop a ::properties_t
 * @param key the key of the property to set
 * @param value the value to set
 */
void properties_set(properties_t* prop, const char* key, const char* value);

/**
 * Retrieves a property from a ::properties_t, specified by its key.
 * If a property with the specified key cannot be found in <code>prop</code>,
 * then an empty string is returned for value.
 *
 * @param prop a ::properties_t
 * @param key the key of the property to get
 * @param value the value of the property, or an empty string if the property was not set
 */
void properties_get(const properties_t* prop, const char* key, char* value);

/**
 * Creates an INI file with the contents of a ::properties_t.
 *
 * @param prop a ::properties_t
 * @param filename the name of the file to write
 */
void properties_save(const properties_t* prop, const char* filename);

/**
 * Populates a ::properties_t with properties from an INI file.
 *
 * @param prop a ::properties_t that will be filled with properties from the file
 * @param filename the name of the file to read
 */
void properties_load(properties_t* prop, const char* filename);

#ifdef __cplusplus
}
#endif

#endif /* LAPSE_H */
