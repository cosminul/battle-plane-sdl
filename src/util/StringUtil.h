/*
 * string_util.h
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRING_UTIL_H
#define STRING_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

/**
 * Trims leading and trailing spaces from a string.
 * @param in_str the input string
 * @param in_len the length of the input string, in bytes
 * @param out_str the buffer for the output string
 * @param out_len the size of the output buffer
 * @return the length of the trimmed string
 */
size_t my_strim(const char* in_str, size_t in_len, char* out_str, size_t out_len);

#ifdef __cplusplus
}
#endif

#endif /* STRING_UTIL_H */
