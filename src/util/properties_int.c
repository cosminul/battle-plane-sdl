/*
 * lapse_int.c
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

#include "properties_int.h"

#include <stdlib.h>
#include <string.h>

struct _property_node* new_node(const char* key, const char* value) {
	struct _property_node* node = malloc(sizeof(struct _property_node));
	int val_size = strlen(value) + 1;

	/* allocate the property pair */
	node->prop = malloc(sizeof(struct _property));
	/* set the key */
	node->prop->key = malloc(strlen(key) + 1);
	strcpy(node->prop->key, key);
	/* set the value */
	node->prop->value = malloc(val_size);
	strcpy(node->prop->value, value);
	node->prop->val_size = val_size;
	/* initialize the pointer to the next element */
	node->next = NULL;

	return node;
}

void property_replace(struct _property* prop, const char* value) {
	/* make sure there is enough room for the new value */
	int val_size = strlen(value) + 1;
	if(prop->val_size < val_size) {
		prop->value = realloc(prop->value, val_size);
		prop->val_size = val_size;
	}
	/* set the new value */
	strcpy(prop->value, value);
}
