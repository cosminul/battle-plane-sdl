/*
 * lapse_int.h
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAPSE_INT_H
#define LAPSE_INT_H

struct _properties;

struct _property {
	char* key;
	char* value;
	int val_size;
};

struct _property_node {
	struct _property* prop;
	struct _property_node* next;
};

struct _property_node* new_node(const char* key, const char* value);

void property_replace(struct _property* prop, const char* value);

#endif /* LAPSE_INT_H */
