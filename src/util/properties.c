/*
 * lapse.c
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

#include "file_util.h"
#include "properties.h"
#include "properties_int.h"
#include "string_util.h"

#include <stdlib.h>
#include <string.h>

#define PROP_MAX_LINE_LEN 2048

/** A reference-counted linked list of properties */
struct _properties {
	/** A null-terminated array of property nodes */
	struct _property_node* head;
	/** The reference count of this structure */
	int ref_count;
};

void clear(properties_t* prop);

properties_t* properties_create() {
	properties_t* prop = malloc(sizeof(properties_t));
	prop->head = NULL;
	prop->ref_count = 1;
	return prop;
}

properties_t* properties_reference(properties_t* prop) {
	prop->ref_count++;
	return prop;
}

void properties_destroy(properties_t* prop) {
	prop->ref_count--;
	if(prop->ref_count == 0) {
		clear(prop);
		free(prop);
		prop = NULL;
	}
}

void properties_set(properties_t* prop, const char* key, const char* value) {
	struct _property_node* cursor = prop->head;
	struct _property_node* tail = prop->head;
	if(prop->head == NULL) {
		prop->head = new_node(key, value);
		return;
	}
	while(cursor != NULL) {
		if(strcmp(cursor->prop->key, key) == 0) {
			property_replace(cursor->prop, value);
			return;
		}
		tail = cursor;
		cursor = cursor->next;
	}
	cursor = new_node(key, value);
	tail->next = cursor;
}

void properties_get(const properties_t* prop, const char* key, char* value) {
	struct _property_node* cursor = prop->head;
	memset(value, 0, 1); /* default to null string */
	while(cursor != NULL) {
		if(strcmp(cursor->prop->key, key) == 0) {
			strcpy(value, cursor->prop->value);
			return;
		}
	cursor = cursor->next;
	}
}

void clear(properties_t* prop) {
	struct _property_node* cursor = prop->head;
	while((cursor = prop->head) != NULL) {
		free(cursor->prop->key);
		free(cursor->prop->value);
		free(cursor->prop);
		prop->head = cursor->next;
		free(cursor);
	}
}

void properties_save(const properties_t* prop, const char* filename) {
	FILE* out = fopen(filename, "w");
	struct _property_node* cursor = prop->head;
	while(cursor != NULL) {
		const char* k = cursor->prop->key;
		const char* v = cursor->prop->value;
		fprintf(out, "%s = %s\n", k, v);
		cursor = cursor->next;
	}
	fclose(out);
}

void properties_load(properties_t* prop, const char* filename) {
	FILE* in = fopen(filename, "r");
	char line[PROP_MAX_LINE_LEN];
	char key[PROP_MAX_LINE_LEN];
	char value[PROP_MAX_LINE_LEN];
	/* position of the equal sign (key/value separator) */
	char* eqPos = NULL;
	char* tok1 = NULL;
	int tok1_len = 0;
	char* tok2 = NULL;
	int tok2_len = 0;

	clear(prop);
	while(my_fline(in, line, PROP_MAX_LINE_LEN)) {
		/* If the line contains 2 tokens separated by equal sign */
		eqPos = strchr(line, '=');
		if(eqPos != NULL) {
			tok1 = line;
			tok1_len = eqPos - line - 1;
			tok2 = eqPos + 1;
			tok2_len = strlen(line) - tok1_len - 1;
			my_strim(tok1, tok1_len, key, PROP_MAX_LINE_LEN);
			my_strim(tok2, tok2_len, value, PROP_MAX_LINE_LEN);
			properties_set(prop, key, value);
		}
	}
	fclose(in);
}
