#ifndef MY_LIST_H
#define MY_LIST_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * A <code>list_t</code> contains a list of pointers.
 * Memory management of <code>list_t</code> is done with
 * list_reference() and list_destroy().
 */
typedef struct _list list_t;

/**
 * Creates a new ::list_t containing no pointers.
 *
 * @return a newly allocated ::list_t with a reference count of 1.
 */
list_t* list_create();

/**
 * Increases the reference count on <code>list</code> by one.
 * This prevents <code>list</code> from being destroyed
 * until a matching call to list_destroy() is made.
 *
 * @param list a ::list_t
 * @return the referenced ::list_t
 */
list_t* list_reference(list_t* list);

/**
 * Decreases the reference count on <code>list</code> by one.
 * If the result is zero, then <code>list</code> and all associated
 * resources are freed.
 * See list_reference().
 *
 * @param list a ::list_t
 */
void list_destroy(list_t* list);

/**
 * Creates a new list with the same elements as the specified list.
 *
 * @param list the list to copy
 * @return a newly allocated ::list_t with a reference count of 1.
 */
list_t* list_copy(list_t* list);

int list_get_size(list_t* list);

void* list_get(list_t* list, int index);

void list_add(list_t* list, void* data);

void list_remove(list_t* list, void* data);

void list_clear(list_t* list);

#ifdef __cplusplus
}
#endif

#endif /* MY_LIST_H */
