#include <stdlib.h>

#include "list.h"

struct _node {
	void* data;
	struct _node* next;
};

struct _list {
	struct _node* head;
	int size;
	int ref_count;
};

list_t* list_create() {
	list_t* list = malloc(sizeof(list_t));
	list->head = NULL;
	list->size = 0;
	list->ref_count = 1;
	return list;
}

list_t* list_reference(list_t* list) {
	list->ref_count++;
	return list;
}

void list_destroy(list_t* list) {
	list->ref_count--;
	if(list->ref_count == 0) {
		list_clear(list);
		free(list);
		list = NULL;
	}
}

list_t* list_copy(list_t* list) {
	list_t* copy = list_create();
	/* Do only one iteration instead of
	 * repeatedly calling list_add(list_get())
	 */
	struct _node* src_cursor = list->head;
	struct _node* dst_cursor = copy->head; /* ...which is actually null */
	while(src_cursor != NULL) {
		/* Create a new node */
		struct _node* new_node = malloc(sizeof(struct _node));
		new_node->data = src_cursor->data;
		new_node->next = NULL;
		/* Add it to the copy */
		if(dst_cursor == NULL) {
			/* Head */
			copy->head = new_node;
			dst_cursor = copy->head;
		} else {
			dst_cursor->next = new_node;
			dst_cursor = new_node;
		}

		src_cursor = src_cursor->next;
	}
	copy->size = list->size;
	return copy;
}

int list_get_size(list_t* list) {
	return list->size;
}

void* list_get(list_t* list, int index) {
	struct _node* cursor = list->head;
	int i = 0;
	if(index < 0 || index >= list->size) {
		/* Out of bounds */
		return NULL;
	}
	for(i = 0; i < index; i++) {
		cursor = cursor->next;
	}
	return cursor->data;
}

void list_add(list_t* list, void* data) {
	struct _node* cursor = list->head;
	int i = 0;
	/* Create a new node */
	struct _node* new_node = malloc(sizeof(struct _node));
	new_node->data = data;
	new_node->next = NULL;
	if(list->size == 0) {
		list->head = new_node;
	} else {
		for(i = 0; i < list->size - 1; i++) {
			cursor = cursor->next;
		}
		cursor->next = new_node;
	}
	list->size++;
}

void list_remove(list_t* list, void* data) {
	struct _node* cursor = list->head;
	struct _node* prev = NULL;
	while(cursor != NULL) {
		if(cursor->data == data) {
			if(prev == NULL) {
				/* delete head */
				list->head = cursor->next;
			} else {
				/* delete a regular node */
				prev->next = cursor->next;
			}
			cursor->data = NULL;
			prev = cursor;
			free(cursor);
			list->size--;
			return;
		}
		prev = cursor;
		cursor = cursor->next;
	}
}

void list_clear(list_t* list) {
	struct _node* cursor = list->head;
	while((cursor = list->head) != NULL) {
		cursor->data = NULL;
		list->head = cursor->next;
		free(cursor);
		list->size--;
	}
}
