#ifndef FONT_H
#define FONT_H

#include <vector>

#include <SDL/SDL.h>

class Font
{
public:
	Font();
	Font(const Font& other);
	~Font();
	Font& operator=(const Font& other);
	SDL_Surface* getSurface() const;
	/**
	 * The font keeps a reference to surface.
	 * @param surface
	 */
	void setSurface(SDL_Surface* surface);
	std::vector<SDL_Rect> getCharBoxes() const;
	SDL_Rect getCharBox(char character) const;
	void setCharBoxes(const std::vector<SDL_Rect>& charBoxes);
	int getNewLineHeight() const;
	void setNewLineHeight(int newLineHeight);
	int getSpaceWidth() const;
	void setSpaceWidth(int spaceWidth);

private:
	SDL_Surface* surface;
	std::vector<SDL_Rect> charBoxes;
	int newLineHeight;
	int spaceWidth;
};

#endif // FONT_H
