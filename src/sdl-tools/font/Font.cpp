#include "Font.h"

Font::Font():
	surface(NULL),
	charBoxes(256),
	newLineHeight(0),
	spaceWidth(0)
{
}

Font::Font(const Font& other):
	surface(other.surface),
	charBoxes(other.charBoxes),
	newLineHeight(other.newLineHeight),
	spaceWidth(other.spaceWidth)
{
	surface->refcount++;
}

Font::~Font()
{
	SDL_FreeSurface(surface);
}

Font& Font::operator=(const Font& other)
{
	if(this != &other) {
		surface = other.surface;
		surface->refcount++;
		charBoxes = other.charBoxes;
		newLineHeight = other.newLineHeight;
		spaceWidth = other.spaceWidth;
	}
	return *this;
}

SDL_Surface* Font::getSurface() const
{
	return surface;
}

void Font::setSurface(SDL_Surface* surface)
{
	this->surface = surface;
	this->surface->refcount++;
}

std::vector<SDL_Rect> Font::getCharBoxes() const
{
	return charBoxes;
}

SDL_Rect Font::getCharBox(char character) const
{
	return charBoxes.at(character);
}

void Font::setCharBoxes(const std::vector<SDL_Rect>& charBoxes)
{
	this->charBoxes = charBoxes;
}

int Font::getNewLineHeight() const
{
	return newLineHeight;
}

void Font::setNewLineHeight(int newLineHeight)
{
	this->newLineHeight = newLineHeight;
}

int Font::getSpaceWidth() const
{
	return spaceWidth;
}

void Font::setSpaceWidth(int spaceWidth)
{
	this->spaceWidth = spaceWidth;
}
