#ifndef FONTREADER_H
#define FONTREADER_H

#include <vector>

#include <SDL/SDL.h>

#include "Font.h"

class FontReader {
public:
	FontReader();
	FontReader(const FontReader& other);
	virtual ~FontReader();
	FontReader& operator=(const FontReader& other);

	void setSurface(SDL_Surface* surface);
	void setBgColor(SDL_Color color);
	Font getFont();

private:
	void build(Font& font);
	void findLeftSide(int row, int col);
	void findRightSide(int row, int col);
	int findTop(int row, int col, int oldVal);
	int findBottom(int row, int col);
	Uint32 getPixel32(int x, int y);

private:
	SDL_Surface* surface;
	SDL_Color bgColor;
	Uint32 bgPixel;
	std::vector<SDL_Rect> chars;
	int newLine;
	int space;
	int cellWidth;
	int cellHeight;
};

#endif // FONTREADER_H
