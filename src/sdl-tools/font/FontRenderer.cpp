#include "Dimension.h"
#include "Font.h"
#include "FontRenderer.h"
#include "ImageService.h"

FontRenderer::FontRenderer():
	font(NULL),
	imageService(new ImageService)
{
}

FontRenderer::FontRenderer(const FontRenderer& other):
	font(other.font),
	imageService(new ImageService(*other.imageService))
{
}

FontRenderer::~FontRenderer()
{
	delete imageService;
	if(font != NULL) {
		delete font;
	}
}

FontRenderer& FontRenderer::operator=(const FontRenderer& other)
{
	if(this != &other) {
		font = other.font;
		imageService = new ImageService(*other.imageService);

	}
	return *this;
}

void FontRenderer::setFont(const Font& font)
{
	this->font = new Font(font);
}

Dimension FontRenderer::sizeText(const char* text) const {
	int padding = 1;
	int glyphHeight = font->getCharBox('a').h; // all glyphs have the same height
	int x = 0;
	int y = 0;
	int w = 0;
	int h = glyphHeight;
	int lineWidth = 0;
	// Go through the text
	for(int cursor = 0; text[cursor] != '\0'; cursor++) {
		// If the current character is a space
		if(text[cursor] == ' ') {
			// Move over
			x += font->getSpaceWidth();
			lineWidth = x;
		}
		// If the current character is a newline
		else if(text[cursor] == '\n') {
			// Update box height
			h += font->getNewLineHeight() + glyphHeight;
			// Move down
			y += font->getNewLineHeight();
			//Move back
			x = 0;
			lineWidth = x;
		}
		else {
			// Get the ASCII value of the character
			int ascii = (unsigned char)text[cursor];
			x += font->getCharBox(ascii).w + padding;
			lineWidth = x;
		}
		// Update box width
		if(lineWidth > w) {
			w = lineWidth - padding;
		}
	}
	Dimension size(w, h);
	return size;
}

SDL_Surface* FontRenderer::renderText(const char* text) {
	Dimension size = sizeText(text);
	// Create the target surface
	SDL_Surface* textbuf;
	int width = size.getWidth();
	int height = size.getHeight();
	textbuf = imageService->createImage(width, height);
	if(textbuf == NULL) {
		return NULL;
	}

	int x = 0;
	int y = 0;
	// Go through the text
	for(int cursor = 0; text[cursor] != '\0'; cursor++) {
	// If the current character is a space
		if(text[cursor] == ' ') {
			// Move over
			x += font->getSpaceWidth();
		}
		// If the current character is a newline
		else if(text[cursor] == '\n') {
			// Move down
			y += font->getNewLineHeight();
			// Move back
			x = 0;
		}
		else {
			// Get the ASCII value of the character
			int ascii = (unsigned char)text[cursor];
			// Show the character
			SDL_Surface* bitmap = font->getSurface();
			SDL_Rect clip = font->getCharBox(ascii);
			imageService->applySurface(x, y, bitmap, textbuf, &clip);
			// Move over the width of the character with one pixel of padding
			x += clip.w + 1;
		}
	}

	return textbuf;
}
