#ifndef FONTRENDERER_H
#define FONTRENDERER_H

#include <SDL/SDL.h>

class Dimension;
class Font;
class ImageService;

class FontRenderer
{
public:
	FontRenderer();
	FontRenderer(const FontRenderer& other);
	~FontRenderer();
	FontRenderer& operator=(const FontRenderer& other);

	void setFont(const Font& font);

	Dimension sizeText(const char* text) const;
	/**
	 * Render the LATIN1 encoded text onto a new surface.
	 * The caller (you!) is responsible for freeing any returned surface.
	 * @param the LATIN1 null terminated string to render.
	 * @return a pointer to a new SDL_Surface. NULL is returned on errors.
	 */
	SDL_Surface* renderText(const char* text);

private:
	Font* font;
	ImageService* imageService;
};

#endif // FONTRENDERER_H
