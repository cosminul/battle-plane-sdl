#include "FontReader.h"

FontReader::FontReader():
	surface(NULL),
	bgColor(),
	bgPixel(0),
	chars(256),
	newLine(0),
	space(0),
	cellWidth(0),
	cellHeight(0)
{
}

FontReader::FontReader(const FontReader& other):
	surface(other.surface),
	bgColor(other.bgColor),
	bgPixel(other.bgPixel),
	chars(other.chars),
	newLine(other.newLine),
	space(other.space),
	cellWidth(other.cellWidth),
	cellHeight(other.cellHeight)
{
}

FontReader::~FontReader()
{
}

FontReader& FontReader::operator=(const FontReader& other)
{
	if(this != &other) {
		surface = other.surface;
		bgColor = other.bgColor;
		bgPixel = other.bgPixel;
		chars = other.chars;
		newLine = other.newLine;
		space = other.space;
		cellWidth = other.cellWidth;
		cellHeight = other.cellHeight;
	}
	return *this;
}

void FontReader::setSurface(SDL_Surface* surface)
{
	this->surface = surface;
}

void FontReader::setBgColor(SDL_Color color)
{
	bgColor = color;
}

Font FontReader::getFont()
{
	Font font;
	build(font);

	return font;
}

void FontReader::build(Font& font)
{
	// Set the background color
	bgPixel = SDL_MapRGB(surface->format, 0, 0xFF, 0xFF);
	SDL_SetColorKey(surface, SDL_SRCCOLORKEY, bgPixel);

	// Set the cell dimensions
	cellWidth = surface->w / 16;
	cellHeight = surface->h / 16;
	// New line variables
	int top = cellHeight;
	int baseA = cellHeight;

	// The current character we're setting
	int currentChar = 0;
	// Go through the cell rows
	for(int row = 0; row < 16; row++) {
		// Go through the cell columns
		for(int col = 0; col < 16; col++) {
			// Set the character offset
			chars[currentChar].x = cellWidth * col;
			chars[currentChar].y = cellHeight * row;
			// Set the dimensions of the character
			chars[currentChar].w = cellWidth;
			chars[currentChar].h = cellHeight;

			findLeftSide(row, col);
			findRightSide(row, col);
			top = findTop(row, col, top);
			// Find Bottom of A
			if(currentChar == 'A') {
				baseA = findBottom(row, col);
			}
			// Go to the next character
			currentChar++;
		}
	}
	// Calculate space
	font.setSpaceWidth(cellWidth / 2);
	// Calculate new line
	font.setNewLineHeight(baseA - top);
	// Lop off excess top pixels
	for(int t = 0; t < 256; t++) {
		chars[t].y += top;
		chars[t].h -= top;
	}
	font.setCharBoxes(chars);
	font.setSurface(surface);
}

void FontReader::findLeftSide(int row, int col) {
	// Find Left Side
	int currentChar = row * 16 + col;
	// Go through pixel columns
	for(int pCol = 0; pCol < cellWidth; pCol++) {
		// Go through pixel rows
		for(int pRow = 0; pRow < cellHeight; pRow++) {
			// Get the pixel offsets
			int pX = (cellWidth * col) + pCol;
			int pY = (cellHeight * row) + pRow;
			// If a non colorkey pixel is found
			if(getPixel32(pX, pY) != bgPixel) {
				// Set the x offset
				chars[currentChar].x = pX;
				// Break the loops
				pCol = cellWidth;
				pRow = cellHeight;
			}
		}
	}
}

void FontReader::findRightSide(int row, int col) {
	// Find Right Side
	int currentChar = row * 16 + col;
	// Go through pixel columns
	for(int pCol = cellWidth - 1; pCol >= 0; pCol--) {
		//Go through pixel rows
		for(int pRow = 0; pRow < cellHeight; pRow++) {
			//Get the pixel offsets
			int pX = (cellWidth * col) + pCol;
			int pY = (cellHeight * row) + pRow;
			// If a non colorkey pixel is found
			if(getPixel32(pX, pY) != bgPixel) {
				// Set the width
				chars[currentChar].w = (pX - chars[currentChar].x) + 1;
				// Break the loops
				pCol = -1;
				pRow = cellHeight;
			}
		}
	}
}

// find the tallest character
int FontReader::findTop(int row, int col, int top) {
	// Find Top
	// Go through pixel rows
	for(int pRow = 0; pRow < cellHeight; pRow++) {
		// Go through pixel columns
		for(int pCol = 0; pCol < cellWidth; pCol++) {
			// Get the pixel offsets
			int pX = (cellWidth * col) + pCol;
			int pY = (cellHeight * row) + pRow;
			// If a non colorkey pixel is found
			if(getPixel32(pX, pY) != bgPixel) {
				// If new top is found
				if(pRow < top) {
					top = pRow;
				}
				// Break the loops
				pCol = cellWidth;
				pRow = cellHeight;
			}
		}
	}
	return top;
}

int FontReader::findBottom(int row, int col) {
	int baseA = cellHeight - 1;
	// Go through pixel rows
	for(int pRow = cellHeight - 1; pRow >= 0; pRow--) {
		// Go through pixel columns
		for(int pCol = 0; pCol < cellWidth; pCol++) {
			// Get the pixel offsets
			int pX = (cellWidth * col) + pCol;
			int pY = (cellHeight * row) + pRow;
			// If a non colorkey pixel is found
			if(getPixel32(pX, pY) != bgPixel) {
				// Bottom of a is found
				baseA = pRow;
				// Break the loops
				pCol = cellWidth;
				pRow = -1;
			}
		}
	}
	return baseA;
}

Uint32 FontReader::getPixel32(int x, int y)
{
	// Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;
	// Get the pixel requested
	return pixels[(y * surface->w) + x];
}
