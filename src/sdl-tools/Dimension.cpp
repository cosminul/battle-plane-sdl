#include "Dimension.h"

Dimension::Dimension():
width(0),
height(0)
{
}

Dimension::Dimension(int width, int height):
width(width),
height(height)
{
}

int Dimension::getWidth() const {
	return width;
}

void Dimension::setWidth(int width) {
	this->width = width;
}

int Dimension::getHeight() const {
	return height;
}

void Dimension::setHeight(int height) {
	this->height = height;

}
