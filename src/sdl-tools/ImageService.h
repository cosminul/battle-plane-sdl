#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include <SDL/SDL.h>
#include <string>

class ImageService {
public:
	SDL_Surface* createImage(int width, int height);
	SDL_Surface* loadImage(const std::string& filename);
	void applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL);
	void unloadImage(SDL_Surface* image);
};

#endif // IMAGELOADER_H
