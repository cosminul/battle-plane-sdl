#ifndef SPRITEMANAGER_H
#define SPRITEMANAGER_H

#include <list>
#include <map>
#include <string>

#include <SDL/SDL.h>

class ImageService;
class Sprite;

class SpriteManager {
public:
	SpriteManager();
	SpriteManager(const SpriteManager& other);
	virtual SpriteManager& operator=(const SpriteManager& other);
	virtual ~SpriteManager();
	/**
	 * Reads a sprite sheet description file.
	 *
	 * @param filename the name of the file to read
	 * @return true on success, false on failure
	 */
	virtual bool readSpritesheet(const std::string& filename);
	/**
	 * Gets the sprite corresponding to the specified name.
	 *
	 * @param spriteName the name of the sprite
	 * @return the sprite with the specified name
	 */
	virtual const Sprite& getSprite(const std::string& spriteName) const;

private:
	void addSprite(const std::string& name, SDL_Surface* surf, int x, int y, int w, int h);
	bool loadText(const std::string& filename, SDL_Surface* surf);

private:
	static const int MAX_LINE_LEN;
	static const int MAX_TOKEN_LEN;
	static const int ATTR_COUNT;

	enum AttributeType {
		TOKEN_NAME,
		TOKEN_X,
		TOKEN_Y,
		TOKEN_WIDTH,
		TOKEN_HEIGHT
	};

	ImageService* imageService;
	std::map<std::string, Sprite> sprites;
	std::list<SDL_Surface*> surfaces;
};

#endif // SPRITESMANAGER_H
