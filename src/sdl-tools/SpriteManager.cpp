#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "FileUtil.h"
#include "ImageService.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "StringUtil.h"

const int SpriteManager::MAX_LINE_LEN = 512;
const int SpriteManager::MAX_TOKEN_LEN = 128;
const int SpriteManager::ATTR_COUNT = 5;

SpriteManager::SpriteManager():
imageService(new ImageService()),
sprites(),
surfaces()
{
}

SpriteManager::SpriteManager(const SpriteManager& other):
imageService(new ImageService(*other.imageService)),
sprites(other.sprites),
surfaces(other.surfaces)
{
}

SpriteManager& SpriteManager::operator=(const SpriteManager& other) {
	if(this != &other) {
		delete imageService;
		imageService = new ImageService(*other.imageService);
		sprites = other.sprites;
		surfaces = other.surfaces;
	}
	return *this;
}

SpriteManager::~SpriteManager() {
	std::list<SDL_Surface*>::iterator it = surfaces.begin();
	while(it != surfaces.end()) {
		SDL_Surface* surf = *it;
		imageService->unloadImage(surf);
		it++;
	}
	delete imageService;
}

bool SpriteManager::readSpritesheet(const std::string& filename) {
	SDL_Surface* surf = imageService->loadImage(filename + ".png");
	if(surf == NULL) {
		// Error loading surface;
		return false;
	}
	surfaces.push_back(surf);
	bool result = loadText(filename + ".txt", surf);
	return result;
}

const Sprite& SpriteManager::getSprite(const std::string& spriteName) const {
	std::map<std::string, Sprite>::const_iterator cit = sprites.find(spriteName);
	if(cit == sprites.end()) {
		printf("sprite %s not found\n", spriteName.c_str());
		return static_cast<const Sprite&>(Sprite::NULL_SPRITE);
	}
	return static_cast<const Sprite&>(cit->second);
}

void SpriteManager::addSprite(const std::string& name, SDL_Surface* surf, int x, int y, int w, int h) {
	Sprite sprite;
	printf("add sprite %s: %d, %d, %d, %d\n", name.c_str(), x, y, w, h);
	sprite.setSurface(surf);
	sprite.setRectangle(x, y, w, h);
	sprites.insert(std::pair<std::string, Sprite>(name, sprite));
}

bool SpriteManager::loadText(const std::string& filename, SDL_Surface* surf) {
	FILE* in = fopen(filename.c_str(), "r");
	char line[MAX_LINE_LEN]; // the buffer for the entire line
	// expect 5 tokens: 1 name + 2 coordinates + 2 dimensions
	char attrs[ATTR_COUNT][MAX_TOKEN_LEN];
	for(int i = 0; i < ATTR_COUNT; i++) {
		memset(attrs[i], 0, MAX_TOKEN_LEN);
	}
	while(my_fline(in, line, MAX_LINE_LEN)) {
		char* pch = strtok(line, ",");
		int index = 0;
		while(pch != NULL) {
			my_strim(pch, strlen(pch), attrs[index++], MAX_TOKEN_LEN);
			pch = strtok(NULL, ",");
		}
		if(index == ATTR_COUNT) {
			int x = atoi(attrs[TOKEN_X]);
			int y = atoi(attrs[TOKEN_Y]);
			int w = atoi(attrs[TOKEN_WIDTH]);
			int h = atoi(attrs[TOKEN_HEIGHT]);
			addSprite(attrs[TOKEN_NAME], surf, x, y, w, h);
		}
	}
	fclose(in);
	return true;
}
