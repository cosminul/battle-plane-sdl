#ifndef DIMENSION_H
#define DIMENSION_H

class Dimension {
public:
	Dimension();
	Dimension(int width, int height);
	int getWidth() const;
	void setWidth(int width);
	int getHeight() const;
	void setHeight(int height);

private:
	int width;
	int height;
};

#endif // DIMENSION_H
