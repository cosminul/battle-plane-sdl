#include <SDL/SDL_image.h>

#include "ImageService.h"

SDL_Surface* ImageService::createImage(int width, int height) {
	Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif
	SDL_Surface* surface = SDL_CreateRGBSurface(
				SDL_SWSURFACE, width, height, 32,
				rmask, gmask, bmask, amask);
	return surface;
}

SDL_Surface* ImageService::loadImage(const std::string& filename) {
	// Temporary storage for the image that's loaded
	SDL_Surface* loadedImage = NULL;
	// The optimized image that will be used
	SDL_Surface* optimizedImage = NULL;
	// Load the image
	loadedImage = IMG_Load(filename.c_str());
	// If nothing went wrong in loading the image
	if( loadedImage != NULL ) {
		// Create an optimized image
		optimizedImage = SDL_DisplayFormat(loadedImage);
		// Free the old image
		SDL_FreeSurface( loadedImage );
		// If the image was optimized just fine
		if( optimizedImage != NULL ) {
			// Map the color key
			Uint32 colorkey = SDL_MapRGB( optimizedImage->format, 0xFF, 0, 0xFF );
			// Set all pixels of color R 0xFF, G 0, B 0xFF to be transparent
			SDL_SetColorKey(optimizedImage, SDL_SRCCOLORKEY, colorkey);
		}
	}
	// Return the optimized image
	return optimizedImage;
}

void ImageService::unloadImage(SDL_Surface* image) {
	SDL_FreeSurface(image);
}

void ImageService::applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip) {
	// Holds offsets
	SDL_Rect offset;
	// Get offsets
	offset.x = x;
	offset.y = y;
	// Blit
	SDL_BlitSurface(source, clip, destination, &offset);
}
