#ifndef SPRITE_H
#define SPRITE_H

#include <SDL/SDL.h>

class Sprite {
private:
	SDL_Surface* surf;
	SDL_Rect* rect;

public:
	Sprite();
	Sprite(const Sprite& other);
	~Sprite();
	/**
	 * Copy assignment operator.
	 *
	 * @param other another Sprite
	 */
	Sprite& operator=(const Sprite& other);
	SDL_Rect* getRectangle() const;
	void setRectangle(const SDL_Rect* rect);
	void setRectangle(int x, int y, int w, int h);
	SDL_Surface* getSurface() const;
	void setSurface(SDL_Surface* surface);

public:
	static const Sprite NULL_SPRITE;
};

#endif // SPRITE_H
