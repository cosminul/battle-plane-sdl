#include <cstring>

#include "Sprite.h"

const Sprite Sprite::NULL_SPRITE;

Sprite::Sprite():
surf(NULL),
rect(new SDL_Rect)
{
	rect->x = 0;
	rect->y = 0;
	rect->w = 0;
	rect->h = 0;
}

Sprite::Sprite(const Sprite &other):
surf(other.surf),
rect(new SDL_Rect)
{
	rect->x = other.rect->x;
	rect->y = other.rect->y;
	rect->w = other.rect->w;
	rect->h = other.rect->h;
}

Sprite::~Sprite() {
	delete rect;
}

Sprite& Sprite::operator=(const Sprite& other) {
	if(this != &other) {
		surf = other.surf;
		rect->x = other.rect->x;
		rect->y = other.rect->y;
		rect->w = other.rect->w;
		rect->h = other.rect->h;
	}
	return *this;
}

SDL_Rect* Sprite::getRectangle() const {
	return rect;
}

void Sprite::setRectangle(const SDL_Rect* rectangle) {
	this->rect->x = rectangle->x;
	this->rect->y = rectangle->y;
	this->rect->w = rectangle->w;
	this->rect->h = rectangle->h;
}

void Sprite::setRectangle(int x, int y, int w, int h) {
	this->rect->x = x;
	this->rect->y = y;
	this->rect->w = w;
	this->rect->h = h;
}

SDL_Surface* Sprite::getSurface() const {
	return surf;
}

void Sprite::setSurface(SDL_Surface* surface) {
	this->surf = surface;
}
