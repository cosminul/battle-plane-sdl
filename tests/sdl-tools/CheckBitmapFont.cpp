/*
 * check_string_util.c
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>

#include <SDL/SDL.h>

#include "BitmapFont.h"
#include "Environment.h"
#include "ImageService.h"

const int SCREEN_WIDTH = 320;
const int SCREEN_HEIGHT = 240;
const int SCREEN_BPP = 32;

void setup() {
	// Initialize all SDL subsystems
	if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 ) {
		fail("sdl init failure");
	}
	SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );
}

void teardown() {
	SDL_Quit();
}

START_TEST(testLoad)
{
//	fail_unless(len == 2);

	ImageService imageService;
	std::string filename = Environment::getResourcePath("lazyfont.png");
	SDL_Surface* fontSurface = imageService.loadImage(filename);
	BitmapFont f1(fontSurface);
	BitmapFont f2;
	f2.buildFont(fontSurface);
	fail_unless(f1 == f2);
}
END_TEST

Suite* bitmap_font_suite() {
	Suite* s = suite_create("BitmapFont");

	/* Core test case */
	TCase* tc_core = tcase_create("Core");
	tcase_add_checked_fixture(tc_core, setup, teardown);
	tcase_add_test(tc_core, testLoad);
	suite_add_tcase(s, tc_core);

	return s;
}

int main() {
	int number_failed;
	Suite* s = bitmap_font_suite();
	SRunner* sr = srunner_create(s);
	srunner_set_fork_status(sr, CK_NOFORK); /* for debugging only */
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return ((number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE);
}
