#include <check.h>
#include <stdlib.h>

#include "Box.h"

START_TEST(testBox)
{
	Box b;
	fail_unless(b.getX() == 0);
	fail_unless(b.getY() == 0);
	fail_unless(b.getWidth() == 0);
	fail_unless(b.getHeight() == 0);
}
END_TEST

START_TEST(testBoxByValues)
{
	int x = 5;
	int y = 6;
	int w = 10;
	int h = 8;
	Box b(x, y, w, h);
	fail_unless(b.getX() == x);
	fail_unless(b.getY() == y);
	fail_unless(b.getWidth() == w);
	fail_unless(b.getHeight() == h);
}
END_TEST

START_TEST(testBoxCopy)
{
	int x = 5;
	int y = 6;
	int w = 10;
	int h = 8;
	Box b1(x, y, w, h);
	Box b2(b1);
	fail_unless(b2.getX() == x);
	fail_unless(b2.getY() == y);
	fail_unless(b2.getWidth() == w);
	fail_unless(b2.getHeight() == h);
}
END_TEST

START_TEST(testBoxOperatorCopy)
{
	int x = 5;
	int y = 6;
	int w = 10;
	int h = 8;
	Box b1(x, y, w, h);
	Box b2 = b1;
	fail_unless(b2.getX() == x);
	fail_unless(b2.getY() == y);
	fail_unless(b2.getWidth() == w);
	fail_unless(b2.getHeight() == h);
}
END_TEST

START_TEST(testGetX)
{
	Box b1;
	int x = b1.getX();
	fail_unless(x == 0);

	Box b2(10, 10, 20, 30);
	x = b2.getX();
	fail_unless(x == 10);
}
END_TEST

START_TEST(testSetX)
{
	Box b1;
	int x = b1.getX();
	fail_unless(x == 0);

	b1.setX(5);

	x = b1.getX();
	fail_unless(x == 5);
}
END_TEST

START_TEST(testGetY)
{
	Box b1;
	int y = b1.getY();
	fail_unless(y == 0);

	Box b2(10, 10, 20, 30);
	y = b2.getY();
	fail_unless(y == 10);
}
END_TEST

START_TEST(testSetY)
{
	Box b1;
	int y = b1.getY();
	fail_unless(y == 0);

	b1.setY(5);

	y = b1.getY();
	fail_unless(y == 5);
}
END_TEST

START_TEST(testGetWidth)
{
	Box b1;
	int w = b1.getWidth();
	fail_unless(w == 0);

	Box b2(10, 10, 20, 30);
	w = b2.getWidth();
	fail_unless(w == 20);
}
END_TEST

START_TEST(testSetWidth)
{
	Box b1;
	int w = b1.getWidth();
	fail_unless(w == 0);

	b1.setWidth(5);

	w = b1.getWidth();
	fail_unless(w == 5);
}
END_TEST

START_TEST(testGetHeight)
{
	Box b1;
	int h = b1.getHeight();
	fail_unless(h == 0);

	Box b2(10, 10, 20, 30);
	h = b2.getHeight();
	fail_unless(h == 30);
}
END_TEST

START_TEST(testSetHeight)
{
	Box b1;
	int h = b1.getHeight();
	fail_unless(h == 0);

	b1.setHeight(5);

	h = b1.getHeight();
	fail_unless(h == 5);
}
END_TEST

START_TEST(testMoveX)
{
	// Initialize position
	Box b(5, 5, 10, 8);
	int x = b.getX();
	int w = b.getWidth();
	fail_unless(x == 5);
	fail_unless(w == 10);
	// Move 3 units to the right
	b.moveX(3);
	x = b.getX();
	w = b.getWidth();
	fail_unless(x == 5 + 3);
	fail_unless(w == 10);
	// Move 2 units to the left
	b.moveX(-2);
	x = b.getX();
	w = b.getWidth();
	fail_unless(x == 5 + 3 - 2);
	fail_unless(w == 10);
}
END_TEST

START_TEST(testMoveY)
{
	// Initialize position
	Box b(5, 5, 10, 8);
	int y = b.getY();
	int h = b.getHeight();
	fail_unless(y == 5);
	fail_unless(h == 8);
	// Move 3 units down
	b.moveY(3);
	y = b.getY();
	h = b.getHeight();
	fail_unless(y == 5 + 3);
	fail_unless(h == 8);
	// Move 2 units up
	b.moveY(-2);
	y = b.getY();
	h = b.getHeight();
	fail_unless(y == 5 + 3 - 2);
	fail_unless(h == 8);
}
END_TEST

START_TEST(testCollides)
{
	Box b1(0, 0, 10, 10);
	Box b2(5, 5, 10, 10);
	fail_unless(b1.collides(b2));

	Box b3(0, 0, 10, 10);
	Box b4(15, 15, 10, 10);
	fail_if(b3.collides(b4));
}
END_TEST

START_TEST(testTouches)
{
	Box b1(0, 0, 10, 10);
	Box b2(0, 10, 10, 10);
	fail_unless(b1.touches(b2));
	fail_if(b1.collides(b2));

	Box b3(0, 0, 10, 10);
	Box b4(15, 15, 10, 10);
	fail_if(b3.touches(b4));
}
END_TEST

Suite* boxSuite() {
	Suite* s = suite_create("Box");

	/* Core test case */
	TCase* tc_core = tcase_create("Core");
	tcase_add_test(tc_core, testBox);
	tcase_add_test(tc_core, testBoxByValues);
	tcase_add_test(tc_core, testBoxCopy);
	tcase_add_test(tc_core, testBoxOperatorCopy);
	tcase_add_test(tc_core, testGetX);
	tcase_add_test(tc_core, testSetX);
	tcase_add_test(tc_core, testGetY);
	tcase_add_test(tc_core, testSetY);
	tcase_add_test(tc_core, testGetWidth);
	tcase_add_test(tc_core, testSetWidth);
	tcase_add_test(tc_core, testGetHeight);
	tcase_add_test(tc_core, testSetHeight);
	tcase_add_test(tc_core, testMoveX);
	tcase_add_test(tc_core, testMoveY);
	tcase_add_test(tc_core, testCollides);
	tcase_add_test(tc_core, testTouches);
	suite_add_tcase(s, tc_core);

	return s;
}

int main() {
	int number_failed;
	Suite* s = boxSuite();
	SRunner* sr = srunner_create(s);
	srunner_set_fork_status(sr, CK_NOFORK); /* for debugging only */
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return ((number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE);
}
