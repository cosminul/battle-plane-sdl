/*
 * check_string_util.c
 * Copyright 2011 Cosmin Humeniuc
 *
 * This file is part of lapse - lightweight application settings.
 *
 * lapse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lapse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lapse. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>
#include <stdlib.h>

#include "StringUtil.h"

#define OUT_BUFF_LEN 32

START_TEST(test_my_strim)
{
	char out[OUT_BUFF_LEN];
	size_t len = 0;

	/* "abc" trimmed is "abc", with a length of 3 */
	len = my_strim("abc", strlen("abc"), out, OUT_BUFF_LEN);
	fail_unless(strcmp(out, "abc") == 0);
	fail_unless(len == 3);

	/* " a " trimmed is "a", with a length of 1 */
	len = my_strim(" a ", strlen(" a "), out, OUT_BUFF_LEN);
	fail_unless(strcmp(out, "a") == 0);
	fail_unless(len == 1);

	/* " abc " trimmed is "abc", with a length of 3 */
	len = my_strim(" abc ", strlen(" abc "), out, OUT_BUFF_LEN);
	fail_unless(strcmp(out, "abc") == 0);
	fail_unless(len == 3);

	/* anything trimmed using a buffer length of 0 is an empty string */
	len = my_strim(" abc ", strlen(" abc "), out, 0);
	fail_unless(strcmp(out, "") == 0);
	fail_unless(len == 0);

	/* test trimming substrings */
	len = my_strim(" abc ", 3, out, OUT_BUFF_LEN);
	fail_unless(strcmp(out, "ab") == 0);
	fail_unless(len == 2);

	/* test trimming substrings */
	len = my_strim(" ab  c ", 5, out, OUT_BUFF_LEN);
	fail_unless(strcmp(out, "ab") == 0);
	fail_unless(len == 2);
}
END_TEST

Suite* string_util_suite() {
	Suite* s = suite_create("StringUtil");

	/* Core test case */
	TCase* tc_core = tcase_create("Core");
	tcase_add_test(tc_core, test_my_strim);
	suite_add_tcase(s, tc_core);

	return s;
}

int main() {
	int number_failed;
	Suite* s = string_util_suite();
	SRunner* sr = srunner_create(s);
	srunner_set_fork_status(sr, CK_NOFORK); /* for debugging only */
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return ((number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE);
}
